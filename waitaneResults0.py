#!/usr/bin/env python

import os
import pickle
from LAS_Scripts import postProcessing

######################
# Main function
def main():



    ##################################################
    ## TODO: RE-DEVELOP THE COMBINATION OF WL AND LAS
    combineWLResults = False
    imageYear = None
    wildlifeYearRange = None
    ##################################################


    # set paths to scripts and data
    outputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 'LAS', 
            'Waitane', 'Results', 'Model0')

    inputResults = os.path.join(outputDataPath, 'out_results.pkl')
    fileobj = open(inputResults, 'rb')
    resultsdata = pickle.load(fileobj)
    fileobj.close()

    extentLineFname = os.path.join(outputDataPath, 'extentLine.shp')      # vcz extent Line shapefile

    # ResultsProcessing
    resultsprocess = postProcessing.ResultsProcessing(resultsdata, 
        outputDataPath, extentLineFname)


    ## TODO: DEVELOP THIS WITH NEW WEIGHTED SSE APPROACH.
    # Combine Data
    # need an on/off switch for user to decide to combine LAS and WL
    if combineWLResults:
        combinedResults = postProcessing.CombineData(resultsdata, resultsprocess)



    
if __name__ == '__main__':
    main()
   
