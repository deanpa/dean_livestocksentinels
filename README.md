# Proof of Freedom - Livestock as Sentinels #

This Python module simulates the movement of wildlife vectors between farms, allowing ADM users to use livestock to predict the probability of the disease spreading.

It is part of the Proof of Freedom suite, which is used to gather evidence that an area is disease-free.

### How do I get set up? ###

Install Python 3.4+ (TODO - determine if 64-bit required).

If you're behind the OSPRI firewall, you may need to install CNTLM so that pip can authenticate to the proxy.

## Step to run locally
1.	Install conda on windows by downloading the python 3.7 version
     https://repo.anaconda.com/archive/Anaconda3-2019.03-Windows-x86_64.exe
2.	Clone the project from bit bucket
    https://bitbucket.org/deanpa/dean_livestocksentinels/overview
3.	Go to the project folder and create the conda environment for the project by running
    conda env create --file=livestockSentinelsEnv.yml
4.	Activate the conda environment by running:
    Condo activate livestockSentinelsEnv
5.	Run your  model inside the environment IwaitaneResult0.py or waitaneModel.py) e.g python waitaneModel.py


### Contribution guidelines ###

This module is developed in conjunction with Landcare Research. Code changes should be developed in a branch so they can be submitted as pull requests and reviewed.

### Who do I talk to? ###

* @deanpa - Landcare Research developer
* @barstowj - OSPRI developer
* @OSPRITBFree (John Rusk) - OSPRI tech lead