#!/usr/bin/env python

import numpy as np
import pylab as P




class SimSensitivity():
    def __init__(self):
        """
        set model parameters and variables
        """
        self.pStar = np.array([1., 2., 4., 8., 16., 32.])
        self.iMax = np.array([0.05, 0.10, 0.15, 0.2, 0.25, .30])
        self.percentSurvey = np.array([0.15, 0.30, 0.45, 0.60, 0.75, 0.90])
        self.pTest = np.array([0.40, 0.50, 0.60, 0.70, 0.80, 0.90])
        self.stocking = np.array([1.0, 2.0, 3.0, 4.0, 5.0, 6.0])

        self.np = len(self.pStar)
        self.area = 10000.0

        ####### Run functions
        # calc sensitivity arrays
        self.calcSSe()
        self.calcProportion()
        self.plotSensitivity()
#        self.wrapStage2()

    ######## Class functions

    def wrapStage2(self):
        """
        Wrapper function for stage II
        """
        self.stage2SSe()
        self.plotStage2()

    def stage2SSe(self):
        """
        simulate SSe for different rates of increase of Pstar for
        stage II eradication
        """
        # array to populate with SSe
        self.SSePStar = np.zeros((14, 4))
        # loop to read in data and populate
        for i in range(4):
            if i == 0:
                # read in no increase results
                fname = 'LAS_PoF_SSe_Table.txt'
            else:
                fname = 'LAS_PoF_SSe_Table_' + str(i) + '.txt'
            data0 = np.genfromtxt(fname, delimiter=None, names=True,
                dtype=['i8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8', 'f8'])
            sseI = data0['Mean_SSe']
#            print('sseI', sseI)
            self.SSePStar[:, i] = sseI
        print('ssePstar', self.SSePStar)

    def plotStage2(self):
        """
        plot SSe in stage II
        """
        self.years = np.arange(2016, 2030)
        P.figure(figsize=(8, 6))
        npstar = np.shape(self.SSePStar)[1]
        lColor = np.array(['k', 'y', 'b', 'r'])
        print(np.shape(self.SSePStar), npstar)
        for i in range(npstar):
            lName = 'P* rate of increase = ' + str(i)
            colorName = lColor[i]
            sseI = self.SSePStar[:, i]
            print(sseI)
            P.plot(self.years, sseI, label = lName, color = colorName, linewidth = 4)
        P.xlabel('Years', fontsize = 12)
        P.ylabel('System sensitivity', fontsize = 12)
        P.legend(loc='upper left')
        P.savefig('pStarIncrease.png', format='png')
        P.show()
        



    def calcProportion(self):
        """
        calc proportional change in sensitivity
        """
        self.pStarPropSSe = self.pStarSSe/self.pStarSSe[1]
        self.iMaxPropSSe = self.iMaxSSe/self.iMaxSSe[1]
        self.percentSurveyPropSSe = self.percentSurveySSe/self.percentSurveySSe[1]
        self.pTestPropSSe = self.pTestSSe/self.pTestSSe[1]
        self.stockingPropSSe = self.stockingSSe / self.stockingSSe[1]        

        self.pStarProp = self.pStar/self.pStar[1]
        self.iMaxProp = self.iMax/self.iMax[1]
        self.percentSurveyProp = self.percentSurvey/self.percentSurvey[1]
        self.pTestProp = self.pTest/self.pTest[1]
        self.stockingProp = self.stocking / self.stocking[1]        


    def plotSensitivity(self):
        """
        plot sensitivity results
        """

        P.figure(figsize=(6, 6))
        P.plot(self.pStarProp, self.pStarPropSSe, label = 'P*', color = 'k', linewidth = 5)
        P.plot(self.iMaxProp, self.iMaxPropSSe, label = 'Imax', color = 'b', linewidth = 5)
        P.plot(self.percentSurveyProp, self.percentSurveyPropSSe, label = '% survey',
             color = 'y', linewidth = 5)
        P.plot(self.pTestProp, self.pTestPropSSe, label = 'P(Test)', color = 'r', linewidth = 5)
        maxX = np.max(self.pStarProp)
        maxY = np.max(self.pStarPropSSe)
        P.xlim(0, maxX)
        P.ylim(0, maxY)
        P.xlabel('Proportional change in parameter', fontsize = 12)
        P.ylabel('Proportional change in SSe', fontsize = 12)
        P.legend(loc='upper left')
#        P.savefig('SensitivityAnalysis.png', format='png')
        P.show()

    def calcSSe(self):
        """
        calc sse for sensitivity test
        """
        # pStar
        moveArea = self.percentSurvey[1] * self.area 
        pInfect = self.iMax[1]/moveArea
        seuij = pInfect * self.pTest[1]
        nTested = self.stocking[1] * moveArea
        seuAve = 1.0 - (1.0 - seuij)**nTested
        self.pStarSSe = 1.0 - (1.0 - (seuAve * self.percentSurvey[1]))**self.pStar
        print('######### pStar')
#        print('pinfect', pInfect, 'seuij', seuij, 'ntested', nTested, 'seuAve', seuAve)
        print(self.pStarSSe)

        # iMax
        moveArea = self.percentSurvey[1] * self.area 
        pInfect = self.iMax/moveArea
        seuij = pInfect * self.pTest[1]
        nTested = self.stocking[1] * moveArea
        seuAve = 1.0 - (1.0 - seuij)**nTested
        self.iMaxSSe = 1.0 - (1.0 - (seuAve * self.percentSurvey[1]))**self.pStar[1]        
        print('############# iMax')
        print(self.iMaxSSe)

        # percent survey
        moveArea = self.percentSurvey * self.area 
        pInfect = self.iMax[1]/moveArea
        seuij = pInfect * self.pTest[1]
        nTested = self.stocking[1] * moveArea
        seuAve = 1.0 - (1.0 - seuij)**nTested
        self.percentSurveySSe = 1.0 - (1.0 - (seuAve * self.percentSurvey))**self.pStar[1]
        print('############# percent survey')
        print(self.percentSurveySSe)

        # pTest
        moveArea = self.percentSurvey[1] * self.area 
        pInfect = self.iMax[1]/moveArea
        seuij = pInfect * self.pTest
        nTested = self.stocking[1] * moveArea
        seuAve = 1.0 - (1.0 - seuij)**nTested
        self.pTestSSe = 1.0 - (1.0 - (seuAve * self.percentSurvey[1]))**self.pStar[1]
        print('############# pTest')
        print(self.pTestSSe)

        # stocking
        moveArea = self.percentSurvey[1] * self.area 
        pInfect = self.iMax[1]/moveArea
        seuij = pInfect * self.pTest[1]
        nTested = self.stocking * moveArea
        seuAve = 1.0 - (1.0 - seuij)**nTested
        self.stockingSSe = 1.0 - (1.0 - (seuAve * self.percentSurvey[1]))**self.pStar[1]
        print('############# stocking')
        print(self.stockingSSe)




def main():

    simsensitivity = SimSensitivity()


if __name__ == '__main__':
    main()


