
"""
Contains class that allow setting the numeric parameters that
control the running of the model.
"""

# This file is part of Livestock as Sentinels model for TB
# Copyright (C) 2018 Dean Anderson
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#from __future__ import print_function, division
import numpy as np
from numba import jit

#PERT_SHAPE = 4.0
#"default pert distribution shape"


class LASParameters(object):
    """
    Contains the parameters for the POA run. This object is also required 
    for the pre-processing. 
    """
    PStar = 1.0
    annual_PStar_Increase = 0.0
    prior = np.array([.5, .5])
    pIntro = np.array([.5, .5])
    niter = 10
    HabitatThreshold = 0
    Imax = np.array([0.1, .01])
    sigma_mean = np.array([90.0, 10.0])
    resol = 100
    years = None
    cattleFieldTest = None
    cattleAbattoir  = None
    deerFieldTest = None
    deerAbattoir = None


    def __init__(self):
        """
        Set model parameters
        set of animals/traps isn't appropriate.
        """
    def setHabitatThreshold(self, HabitatThreshold):
        """
        This sets the minimum RR - values below this are ignored
        """
        self.HabitatThreshold = HabitatThreshold

    def setYears(self, years):
        """
        Sets a sequence of years that the model is to be run over
        """
        self.years = years

    def setNumIterations(self, niter):
        """
        Set the number of iterations to do for each year
        """
        self.niter = niter    

    def setResol(self, resol):
        """
        set pixel resolution in metres
        """
        self.resol = resol

    def setPrior(self, prior):
        """
        set prior
        """
        self.prior = prior

    def setPIntro(self, pIntro):
        """
        set the probability of introduction
        """
        self.pIntro = pIntro

    def setNoGoThreshold(self, noGoThreshold):
        """
        habitat where livestock cannot go, i.e. dense bush
        """
        self.noGoThreshold = noGoThreshold

    def setPStar(self, PStar):
        """
        set PStar
        """
        self.PStar = PStar

    def setAnnual_PStar_Increase(self, annual_PStar_Increase):
        self.annual_PStar_Increase = annual_PStar_Increase

    def setImax(self, Imax):
        self.Imax = Imax

    def setSigma_mean(self, sigma_mean):
        """
        # sigma mean and sd
        """
        self.sigma_mean = sigma_mean

    ###############
    # Testing Sensitivities
    def setCattleFieldTest(self, cattleFieldTest):
        self.cattleFieldTest = cattleFieldTest

    def setCattleAbattoir(self, cattleAbattoir):
        self.cattleAbattoir = cattleAbattoir

    def setDeerFieldTest(self, deerFieldTest):
        self.deerFieldTest = deerFieldTest

    def setDeerAbattoir(self, deerAbattoir):
        self.deerAbattoir = deerAbattoir
