#!/usr/bin/env python

import os
import numpy as np
import pickle
from osgeo import gdal
from osgeo import ogr
from osgeo import gdalconst
from numba import jit
from . import params


########################################
#      Functions
########################################

@jit
def LooperSeU(niter, SeU, SSe, PoF, sumSeU, nyears, extMask, farmExtentMask, periphFarmMask,
              distDecayRaster, priorI, pIntroI, sigmaI, cattleFieldSe,
              cattleAbattoirSe, deerFieldSe, deerAbattoirSe, pInfectAccess, nfarms,
              testDat, sentinelType, nTestByYear, nrows, ncols, prpSearched, nSearched,
              EPIAve, Ntotal, seuAveLAS):
    """
    # calc SSe and PoF over extent over all years and iterations
    """
    # loop thru iterations
    for i in range(niter):
        # get initial prior - to be updated
        pFree = priorI[i]
        # loop thru years
        for j in range(nyears):
            # index for indexing the test data columns by year
            cid = j * 2
            # loop thru cells
            for x in range(ncols):
                for y in range(nrows):
                    # reset SeU
                    SeU[y, x] = 0.0
                    # if in extent of VCZ
                    if extMask[y, x] == 1:
                        # loop thru farms
                        for k in range(nfarms):
                            # if herd was tested, proceed
                            if nTestByYear[k, j] > 0:
                                # get seu in accessible cells for each sentinel and test type
                                SeUAccessCF = cattleFieldSe[i] * pInfectAccess[i, k]
                                SeUAccessCA = cattleAbattoirSe[i] * pInfectAccess[i, k]
                                SeUAccessDF = deerFieldSe[i] * pInfectAccess[i, k]
                                SeUAccessDA = deerAbattoirSe[i] * pInfectAccess[i, k]
                                # Get seu in areas where animals can go and possums can live
                                if farmExtentMask[k, y, x] == 1:
                                    # if cattle
                                    if sentinelType[k] == 0:
                                        seuField = 1.0 - ((1.0 - SeUAccessCF)**(testDat[k, cid]))
                                        seuAbattoir = 1.0 - ((1.0 - SeUAccessCA)**(testDat[k, (cid + 1)]))
                                    # deer
                                    else:
                                        seuField = 1.0 - ((1.0 - SeUAccessDF)**(testDat[k, cid]))
                                        seuAbattoir = 1.0 - ((1.0 - SeUAccessDA)**(testDat[k, (cid + 1)]))
                                    seuTotal = 1.0 - ((1.0 - seuField) * (1.0 - seuAbattoir))
                                    SeU[y, x] = 1.0 - ((1.0 - SeU[y, x]) * (1.0 - seuTotal))
                                    # make SeU-Mean raster: "meanSeU" is 3-d raster, one for each year j
                                # if cell is in farm periphery
                                elif periphFarmMask[k, y, x] == 1:

                                    # get sigmaDecay value
                                    sigmaDecay = np.exp(-distDecayRaster[k, y, x] / (sigmaI[i]**2))
                                    # if cattle
                                    if sentinelType[k] == 0:
                                        # multiply by spatial decay
                                        seuField1 = SeUAccessCF * sigmaDecay
                                        seuField2 = 1.0 - ((1.0 - seuField1)**(testDat[k, cid]))
                                        seuAbattoir1 = SeUAccessCA * sigmaDecay
                                        seuAbattoir2 = 1.0 - ((1.0 - seuAbattoir1)**(testDat[k, (cid + 1)]))
                                    # deer
                                    else:
                                        seuField1 = SeUAccessDF * sigmaDecay
                                        seuField2 = 1.0 - ((1.0 - seuField1)**(testDat[k, cid]))
                                        seuAbattoir1 = SeUAccessDA * sigmaDecay
                                        seuAbattoir2 = 1.0 - ((1.0 - seuAbattoir1)**(testDat[k, (cid + 1)]))
                                    # get total seu for field and abattoir
                                    seuTotal = 1.0 - ((1.0 - seuField2) * (1.0 - seuAbattoir2))
                                    SeU[y, x] = 1.0 - ((1.0 - SeU[y, x]) * (1.0 - seuTotal))
                        # make SeU-Mean raster: "meanSeU" is 3-d raster, one for each year j
                        sumSeU[j, y, x] += SeU[y, x]
            # calc mean SeUAve across all searched cells in year j across all farms
            SeUAve = np.sum(SeU) / nSearched[j]
            # populate the seuAveLAS array for combined wl-las calculations
            seuAveLAS[i, j] = np.sum(SeU) / Ntotal
            # exponent term for SSe calc
            expTerm = EPIAve[j] * Ntotal
            # calc SSe and PoF; prpSearched is in year j across all farms
            SSePre = 1.0 - ((1.0 - SeUAve * prpSearched[j])**expTerm)
            # system sensitivity in iteration i (row) and year j (col)
            SSe[i, j] = SSePre
            pofPre = pFree / (1.0 - (SSePre * (1.0 - pFree)))
            # probability of freedom in iteration i and year j
            PoF[i, j] = pofPre
            pFree = pofPre * (1.0 - pIntroI[i])
    # return arrays
    return (SSe, PoF, sumSeU, seuAveLAS)


def get_beta(pmean, psd, nn):
    """
    get alpha and beta values frm mean and sd
    """
    a = pmean * (((pmean * (1.0 - pmean)) / (psd**2.0)) - 1.0)
    b = (1.0-pmean)*((pmean*(1.0-pmean))/psd**2.0 - 1.0)
    p = np.random.beta(a, b, nn)
    return p


###########################################
# End Global functions
###########################################


#######################################################################
# Process data class functions and elements
class ProcessData():
    def __init__(self, params, rawdata):
        """
        Process the data in model
        """
        self.params = params
        self.rawdata = rawdata
        ##############################
        #  Run ProcessData functions
        # calculate SeU
        self.WrapperSeU()
        self.makeHerdSeuTable()
        #  End ProcessData-function calls
        ##############################

    ###############################
    #  ProcessData functions
    def WrapperSeU(self):
        """
        # calculate SeU for all cells in extMask
        # loop through farms get seu in farm and in peripheral areas
        """
        # make storage arrays "SSe" and "PoF"
        self.makeStorageArrays()
        # Get random parameter variates
        self.getParameters()
        # loop thru iter, years, cells and farms
        (self.SSe, self.PoF, self.sumSeU, self.seuAveLAS) = LooperSeU(self.params.niter, self.SeU,
            self.SSe, self.PoF, self.sumSeU, self.rawdata.nyears, self.rawdata.extMask,
            self.rawdata.farmExtentMask, self.rawdata.periphFarmMask, self.rawdata.distDecayRaster,
            self.priorI, self.pIntroI, self.sigmaI, self.cattleFieldSe,
            self.cattleAbattoirSe, self.deerFieldSe, self.deerAbattoirSe, self.pInfectAccess,
            self.rawdata.nFarms, self.rawdata.testDat, self.rawdata.sentinelType,
            self.rawdata.nTestByYear, self.rawdata.rows, self.rawdata.cols,
            self.rawdata.prpSearched, self.rawdata.nSearched, self.rawdata.EPIAve,
            self.rawdata.Ntotal, self.seuAveLAS)
        # make meanSeU 3-d raster for post processing
        self.meanSeU = self.sumSeU / self.params.niter

    def makeStorageArrays(self):
        """
        make storage arrays for SSe, and PoF
        """
        # arrays of SSe and PoF - iters by nyears
        self.SSe = np.zeros((self.params.niter, self.rawdata.nyears))
        self.PoF = np.zeros((self.params.niter, self.rawdata.nyears))
        self.SeU = np.zeros((self.rawdata.rows, self.rawdata.cols))
        self.sumSeU = np.zeros((self.rawdata.nyears, self.rawdata.rows, self.rawdata.cols))
        self.seuAveLAS = np.zeros((self.params.niter, self.rawdata.nyears))

    def getParameters(self):
        """
        Draw variates from parameter distributions
        """
        # initial probability of freedom
        self.priorI = get_beta(self.params.prior[0], self.params.prior[1], self.params.niter)
        # annual probability of introduction
        self.pIntroI = get_beta(self.params.pIntro[0], self.params.pIntro[1], self.params.niter)
        # max probability of infection
        self.iMaxI = get_beta(self.params.Imax[0], self.params.Imax[1], self.params.niter)
        # sigma mean and sd
        self.sigmaI = np.random.normal(self.params.sigma_mean[0],
                        self.params.sigma_mean[1], size=self.params.niter)
        # get random variates and calc test Se
        self.getTestSe()
        # calc pInfectAccess
        self.pInfectAccess = np.outer(self.iMaxI, (1.0 / self.rawdata.farmArea))

    def getTestSe(self):
        """
        get random variates and calc test Se
        """
        # calc test Se done in series
        self.cattleFieldSe = np.zeros(self.params.niter)
        self.cattleAbattoirSe = np.zeros(self.params.niter)
        self.deerFieldSe = np.zeros(self.params.niter)
        self.deerAbattoirSe = np.zeros(self.params.niter)
        # loop through iters and populate arrays
        for i in range(self.params.niter):
            self.cattleFieldSe[i] = np.prod(get_beta(self.params.cattleFieldTest[:, 0],
                        self.params.cattleFieldTest[:, 1], self.params.cattleFieldTest.shape[0]))
            self.cattleAbattoirSe[i] = np.prod(get_beta(self.params.cattleAbattoir[:, 0],
                        self.params.cattleAbattoir[:, 1], self.params.cattleAbattoir.shape[0]))
            self.deerFieldSe[i] = np.prod(get_beta(self.params.deerFieldTest[:, 0],
                        self.params.deerFieldTest[:, 1], self.params.deerFieldTest.shape[0]))
            self.deerAbattoirSe[i] = np.prod(get_beta(self.params.deerAbattoir[:, 0],
                        self.params.deerAbattoir[:, 1], self.params.deerAbattoir.shape[0]))


    def makeHerdSeuTable(self):
        """
        make table of mean SeU for each farm and year. 
        Mean is mean self.meanSeU across cells
        Do it here to avoid pickling 'possumOnFarmMask'
        """
        # empty table to populate
        nrow = self.rawdata.nFarms + 1
        ncol = self.rawdata.nyears + 1
        self.farmSeuTable = np.zeros((nrow, ncol))
        # loop thru years
        for i in range(self.rawdata.nyears):
            # 2-d raster of meanSeu in year i across iterations for each pixel
            meanSeU_i = self.meanSeU[i]
            # loop thru each herd
            count = 0
            for j in range(self.rawdata.nFarms):
                # 2-d raster of TB risk areas on Farm j
                possOnFarmMask_j = self.rawdata.possumOnFarmMask[j] == 1
                # calc mean only if cells on farm are at risk
                if np.sum(possOnFarmMask_j) > 0:
                    # meanSeu on farm j in year i
                    meanSeU_ij = meanSeU_i[possOnFarmMask_j]
                    meanMeanSeU_ij = np.mean(meanSeU_ij)
                    # populate table
                    self.farmSeuTable[j, i] = meanMeanSeU_ij

    #   End ProcessData functions
    ###############################

##################################################
# Pickle results to directory
##################################################


class ResultsData(object):
    def __init__(self, rawdata, processdata, params):
        self.nyears = rawdata.nyears
        self.years = rawdata.years
        self.nFarms = rawdata.nFarms
        self.Ntotal = rawdata.Ntotal
        self.SSe = processdata.SSe
        self.PoF = processdata.PoF
        self.meanSeU = processdata.meanSeU
        self.seuAveLAS = processdata.seuAveLAS
        self.prpSearched = rawdata.prpSearched
###        self.ownerID = rawdata.ownerID
        self.herdID = rawdata.herdID
        self.pStarPrp = rawdata.pStarPrp
        self.pStarArray = rawdata.pStarArray
        self.AR = rawdata.AR
        self.extMask = rawdata.extMask
        self.farmSeuTable = processdata.farmSeuTable
        self.xmin = rawdata.xmin
        self.xmax = rawdata.xmax
        self.ymin = rawdata.ymin
        self.ymax = rawdata.ymax
        self.cols = rawdata.cols
        self.rows = rawdata.rows
        self.match_geotrans = rawdata.match_geotrans
        self.extentPolyFname = rawdata.extentPolyFname
        self.prior = params.prior 
        self.years = params.years
        self.niter = params.niter
        self.HabitatThreshold = params.HabitatThreshold
 


