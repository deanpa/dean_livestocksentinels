#!/usr/bin/env python

import os
import numpy as np
import matplotlib.pyplot as P
from scipy.stats.mstats import mquantiles
import prettytable
import pickle
from osgeo import gdal
from osgeo import gdalconst
from osgeo import ogr
from mpl_toolkits.axes_grid1 import make_axes_locatable
#from . import params


# definition stolen from remote sensing files. Note this should be all one line in text editor.
NZTM_WKT = 'PROJCS["NZGD2000 / New Zealand Transverse Mercator 2000",GEOGCS["NZGD2000",DATUM["New_Zealand_Geodetic_Datum_2000",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6167"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4167"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",173],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",1600000],PARAMETER["false_northing",10000000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2193"]]'


def get_beta(pmean, psd, nn):
    """
    get alpha and beta values frm mean and sd
    """
    a = pmean * (((pmean * (1.0 - pmean)) / (psd**2.0)) - 1.0)
    b = (1.0-pmean)*((pmean*(1.0-pmean))/psd**2.0 - 1.0)
    p = np.random.beta(a, b, nn)
    return p


def quantileFX(a):
    """
    calc 95% CIs
    """
    return mquantiles(a, prob=[0.025, 0.975], axis=0)


class ResultsProcessing(object):
    def __init__(self, resultsdata, outputDataPath, extentLineFname):

#        self.params = params
        self.resultsdata = resultsdata
        self.outputDataPath = outputDataPath
        # destination line shapefile of extent vcz
        self.extentLineFname = extentLineFname

        ##########################
        # Run obligatory functions
        # set resolution, geometry for plotting and read in tifs
        self.adjustRes()
        self.makeTableFX()
        # Make table with PoF and SSe over years
        # optional functions
        self.writeToFileFX()             # write table to directory
        self.plotFX()              # plot POF and SSe over time
#        self.plotMeanSeU()               # image of SeU for each year
        # image of single year and farm boundaries
#        self.plotOneYearMeanSeu(params.imageYear)
#        # plot kmap and farm boundaries
#        self.plotKmap()
#        self.makeFarmSeuTable()
#        self.writeFarmSeuToFile()
        # write *.tif to directory of meanSeU for each year
#        self.writeMeanSeuTif()
    #########################
    #########################
    #   Class functions
    #
    def getYearStr(self):
        """
        get string array of years
        """
        self.years = np.arange(self.resultsdata.years[0], (self.resultsdata.years[1] + 1))
        self.nYears = len(self.years)
        self.yrNames = self.years.astype('str')
        self.yseq = np.arange(self.resultsdata.nyears)
        # get axes labels for Seu plots
        (self.xlabels_string, self.xlocs, self.ylabels_string,
            self.ylocs) = self.getEastingsNorthings(self.resultsdata.match_geotrans,
            self.resultsdata.cols, self.resultsdata.rows)

    def makeTableFX(self):
        resultTable = np.zeros(shape=(7, self.resultsdata.nyears))
        meanPoFAllYr = np.round(np.mean(self.resultsdata.PoF, axis=0), 3)
        resultTable[0] = meanPoFAllYr
        PoFQuantiles = np.round(quantileFX(self.resultsdata.PoF), 3)
        resultTable[1:3] = PoFQuantiles
        meanSSeAllYr = np.round(np.mean(self.resultsdata.SSe, axis=0), 3)
        resultTable[3] = meanSSeAllYr
        SSeQuantiles = np.round(quantileFX(self.resultsdata.SSe), 3)
        resultTable[4:6] = SSeQuantiles
        resultTable[6] = np.round(self.resultsdata.prpSearched, 3)
        resultTable = resultTable.transpose()
        aa = prettytable.PrettyTable(['Years', 'Mean PoF', 'Lo CI PoF', 'Hi CI PoF',
                                'Mean SSe', 'Lo CI SSe', 'Hi CI SSe', 'Prop. Searched'])
        for i in range(self.resultsdata.nyears):
            name = self.yrNames[i]
            row = [name] + resultTable[i].tolist()
            aa.add_row(row)
        print('##############   Livestock as sentinels Results Table    ###########')
        print(aa)
        self.summaryTable = resultTable.copy()

    ########
    # Write data to file
    def writeToFileFX(self):
        (m, n) = self.summaryTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Years', 'U12'), ('Mean PoF', np.float),
                    ('Lo CI PoF', np.float), ('Hi CI PoF', np.float), ('Mean SSe', np.float),
                    ('Lo CI SSe', np.float), ('Hi CI SSe', np.float),
                    ('Prop. Searched', np.float)])
        # copy data over
        structured['Mean PoF'] = self.summaryTable[:, 0]
        structured['Lo CI PoF'] = self.summaryTable[:, 1]
        structured['Hi CI PoF'] = self.summaryTable[:, 2]
        structured['Mean SSe'] = self.summaryTable[:, 3]
        structured['Lo CI SSe'] = self.summaryTable[:, 4]
        structured['Hi CI SSe'] = self.summaryTable[:, 5]
        structured['Prop. Searched'] = self.summaryTable[:, 6]
        structured['Years'] = self.yrNames
        summaryTableFname = os.path.join(self.outputDataPath, 'LAS_PoF_SSe_Table.txt')
        np.savetxt(summaryTableFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f',
                    '%.4f', '%.4f', '%.4f', '%.4f'], comments='',
                    header='Years Mean_PoF Lo_CI_PoF Hi_CI_PoF Mean_SSe Lo_CI_SSe Hi_CI_SSe Prop_Searched')

    def getNTicks(self):
        """
        get n==5 ticks for plot
        """
        if self.nYears <= 5:
            self.byTicks = 1
        else:
            self.byTicks = np.round(self.nYears / 5.0)
        self.xTicksYr = np.arange(self.resultsdata.years[0], self.resultsdata.years[1], self.byTicks)
        self.xTicksYr = self.xTicksYr.astype(int)
        self.xlabelsYr = [str(x) for x in self.xTicksYr]

    def plotFX(self):
        """
        graph PoF and SSe results
        """
        # graph PoF results.
        P.figure(figsize=(14, 6))
        P.subplot(1, 2, 1)
        P.plot(self.years, self.summaryTable[:, 0], label='Mean PoE', color='k', linewidth=5)
        P.plot(self.years, self.summaryTable[:, 1], label='Credible Intervals',
               linewidth=2, color='k')
        P.plot(self.years, self.summaryTable[:, 2], color='k')
        P.axhline(y=0.95, color='k', ls='dashed')
        miny = np.min(self.summaryTable[:, 1]) - .1
        if miny < 0:
            miny = 0
        P.ylim([miny, 1])
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        self.getNTicks()
        P.xticks(self.xTicksYr, self.xlabelsYr)
        P.xlabel('Years', fontsize=17)
        P.ylabel('Probability of eradication', fontsize=17)
        P.legend(loc='lower right')
        # graph SSe results
        P.subplot(1, 2, 2)
        P.plot(self.years, self.summaryTable[:, 3], label='Mean SSe', color='k', linewidth=5)
        P.plot(self.years, self.summaryTable[:, 4], label='Credible Intervals',
               linewidth=2, color='k')
        P.plot(self.years, self.summaryTable[:, 5], color='k')
        maxy = np.max(self.summaryTable[:, 5]) + .1
        if maxy > 1:
            maxy = 1
        P.ylim([0, maxy])
        P.xlabel('Years', fontsize=17)
        P.ylabel('System sensitivity', fontsize=17)
        P.legend(loc='upper left')
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        P.xticks(self.xTicksYr, self.xlabelsYr)
        # save plots to project directory
        pofSSeGraphFname = os.path.join(self.outputDataPath, 'PoF_SSe_Graph.png')
        P.savefig(pofSSeGraphFname, format='png')
        P.show()

    def plotMeanSeU(self):
        """
        plot Mean Seu by year
        """
        cc = 0
        tmpMax = np.max(self.resultsdata.meanSeU)
        if tmpMax > 0.25:
            maxSeu = 0.25
        else:
            maxSeu = tmpMax
        nfigures = np.int(np.ceil(self.nYears/4.0))
        for i in range(nfigures):
            P.figure(i)
            lastFigure = i == (nfigures - 1)
            fname = 'LAS_MeanSeU_From_' + str(self.years[cc]) + '.png'
            fname2 = os.path.join(self.outputDataPath, fname)
            for j in range(4):
                P.subplot(2, 2, j+1)
                if cc < self.nYears:
                    raster = self.resultsdata.meanSeU[cc]
                    raster[self.resultsdata.extMask == 0] = -9999
                    raster_masked_array = np.ma.masked_where(raster == -9999, raster)
                    cmap = P.cm.jet
                    cmap.set_bad('w', 1.)
                    ax = P.gca()
                    imgplot = ax.imshow(raster_masked_array, cmap=cmap)
                    P.title('LAS Mean SeU:  ' + str(self.years[cc]), fontsize=10)
                    P.xticks(self.xlocs, self.xlabels_string, fontsize=10)
                    P.yticks(self.ylocs, self.ylabels_string, fontsize=10)
                    divider = make_axes_locatable(ax)
                    cax = divider.append_axes("right", size="2%", pad=0.04)
                    cbar = P.colorbar(imgplot, cax=cax)
                    imgplot.set_clim(0.0, maxSeu)
                    cbar.ax.tick_params(labelsize=10)
                    cc = cc + 1
            P.tight_layout(pad=.75, w_pad=.75)
            P.savefig(fname2, format='png')
            P.show(block=lastFigure)

    def writeTmpTif(self, raster, tempTifName, gdt_type):
        """
        write tif to directory
        """
        # write array to tif in directory
        dst_filename = os.path.join(self.outputDataPath, tempTifName)
        dst = gdal.GetDriverByName('GTiff').Create(dst_filename, self.resultsdata.cols,
                    self.resultsdata.rows, 1, gdt_type)
        dst.SetGeoTransform(self.resultsdata.match_geotrans)
        dst.SetProjection(NZTM_WKT)
        band = dst.GetRasterBand(1)
        band.WriteArray(raster)
        del dst  # Flush

    def plotOneYearMeanSeu(self, yr):
        """
        make one image plot of meanSeu for a specified year
        """
        # increase the resolution
        mask = self.years == yr
        self.yid = self.yseq[mask]
        raster = self.resultsdata.meanSeU[self.yid]
        # do this to get rid of 3rd dimension
        raster = raster[0]
        raster[self.resultsdata.extMask == 0] = -9999
        # write tmp tif to change resolution
        self.tempSeuYearFname = os.path.join(self.outputDataPath, 'temp_seu_year.tif')
        self.writeTmpTif(raster, self.tempSeuYearFname, gdt_type = gdalconst.GDT_Float32)
        # tif back in with higher resolution
        raster = self.readTifIncreaseRes(self.tempSeuYearFname)
        # set non-extent to white
        masked_array = np.ma.masked_where(raster == -9999, raster)
        cmap = P.cm.jet
        cmap.set_bad('w', 1.)
        P.figure(figsize=(11, 11))
        ax = P.gca()
        imgplot = ax.imshow(masked_array, cmap=cmap)
        # plot vcz boundary
        self.plotExtentOutline()
        tmpSeu = np.max(raster)
        if tmpSeu > 0.20:
            maxSeu = 0.20
        else:
            maxSeu = tmpSeu
        P.title('LAS Mean SeU:  ' + str(yr), fontsize=10)
        P.xticks(self.xlocs_line, self.xlabels_line, fontsize=10)
        P.yticks(self.ylocs_line, self.ylabels_line, fontsize=10)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="2%", pad=0.04)
        cbar = P.colorbar(imgplot, cax=cax)
        imgplot.set_clim(0.0, maxSeu)
        cbar.ax.tick_params(labelsize=10)
        figFname = os.path.join(self.outputDataPath, ('LAS_MeanSeu_' + str(yr) + '.png'))
        P.savefig(figFname, format='png')
        P.show()

    def plotExtentOutline(self):
        """
        plot extent lines on other images
        """
        # source polygon shapefile: original vcz shapefile
        self.extentPolyFname = self.resultsdata.extentPolyFname
        # read in vcz polyshape, make line shp - write to directory
        # read in vcz polyshape, make line shp - write to directory
        self.poly2line(self.extentPolyFname, self.extentLineFname)
        # rasterise the new line shapefile and write to Tif:
        self.extLineTifFname = os.path.join(self.outputDataPath, 'extentLineRast.tif')
        self.rasterizeLine(self.extentLineFname, self.extLineTifFname)
        # read in *.tif of line raster and make object raster array:
        self.vczLineRaster = self.makeLineRast(self.extLineTifFname)
        # plot the line raster array
        P.imshow(self.vczLineRaster)

    def poly2line(self, input_poly, output_line):
        """
        convert the polygon farm shapefile to line shapefile
        write to directory
        """
        source_ds = ogr.Open(input_poly)    # input polygon shapefile
        source_layer = source_ds.GetLayer()
        srs = source_layer.GetSpatialRef()  # get spatial ref from shape
        shpDriver = ogr.GetDriverByName("ESRI Shapefile")
        if os.path.exists(output_line):
            shpDriver.DeleteDataSource(output_line)
        outDataSource = shpDriver.CreateDataSource(output_line)
        outLayer = outDataSource.CreateLayer(output_line, srs=srs, geom_type=ogr.wkbMultiLineString)
        featureDefn = outLayer.GetLayerDefn()
        # loop thru features in shapefile
        for feat in source_layer:
            geom = feat.GetGeometryRef()
            lgeom = ogr.ForceToMultiLineString(geom)
            outFeature = ogr.Feature(featureDefn)
            outFeature.SetGeometry(lgeom)
            outLayer.CreateFeature(outFeature)
            outLayer.SyncToDisk()

    def rasterizeLine(self, dstLineShape, lineTifFname):
        """
        rasterize the newly created line shapefile
        Creat *.tif, write to directory
        """
        dataset = ogr.Open(dstLineShape)
        fs_layer = dataset.GetLayer()
        fs_ds = gdal.GetDriverByName('GTiff').Create(lineTifFname, self.cols_line,
                        self.rows_line, 1, gdal.GDT_UInt16)
        fs_ds.SetGeoTransform(self.line_geotrans)
        fs_ds.SetProjection(NZTM_WKT)
        band = fs_ds.GetRasterBand(1)
        NoData_value = -9999
        band.SetNoDataValue(NoData_value)
        # Rasterize FarmLine and write to directory
        gdal.RasterizeLayer(fs_ds, [1], fs_layer, burn_values=[1])
        fs_ds.FlushCache()
        del fs_layer
        del dataset

    def makeLineRast(self, lineTifFname):
        """
        make farm line rast for plotting
        """
        lineRastName = gdal.Open(lineTifFname).ReadAsArray()
        # convert self.lineRast to a 4 layer image with a lookup table
        lut = np.zeros((2, 4), np.uint8)
        lut[1] = (0, 0, 0, 255)  # black with alpha =255
        lineRastName = lut[lineRastName]
        return(lineRastName)

    def getEastingsNorthings(self, geotrans, cols, rows):
        """
        get eastings and northings for axes labels
        """
        # assign eastings and northings to axes
        tlx, tly = gdal.ApplyGeoTransform(geotrans, 0, 0)
        brx, bry = gdal.ApplyGeoTransform(geotrans, cols, rows)
        # now make tlx, tly, brx, and bry look 'nice' ie divisible by 1000 etc
        # make sure you round up or down appropriately
        invtransform = gdal.InvGeoTransform(geotrans)
        nlabels = 3
        xlabels = np.linspace(tlx, brx, nlabels)
        xlabels = np.floor(xlabels)
        xlocs = [int(gdal.ApplyGeoTransform(invtransform, x, 0)[0]) for x in xlabels]
        xlabels_string = [str(x) for x in xlabels]
        ylabels = np.linspace(tly, bry, nlabels)
        ylabels = np.floor(ylabels)
        ylocs = [int(gdal.ApplyGeoTransform(invtransform, 0, y)[1]) for y in ylabels]
        ylabels_string = [str(y) for y in ylabels]
        return(xlabels_string, xlocs, ylabels_string, ylocs)

    def readTifIncreaseRes(self, fname):
        """
        Read data out of the first band of the named file
        """
        ds = gdal.Open(fname)
        band = ds.GetRasterBand(1)
        data = band.ReadAsArray(buf_xsize=self.cols_line, buf_ysize=self.rows_line)
        del ds
        return data

    def adjustRes(self):
        """
        adjust resolution for plotting
        """
        self.getYearStr()
        self.resol_line = 50
        self.cols_line = np.int(np.ceil((self.resultsdata.xmax - self.resultsdata.xmin) / self.resol_line))
        self.rows_line = np.int(np.ceil((self.resultsdata.ymax - self.resultsdata.ymin) / self.resol_line))
        self.line_geotrans = [self.resultsdata.xmin, self.resol_line, 0, self.resultsdata.ymax, 0,
                                -self.resol_line]
        # get eastings and northings for axes labels
        (self.xlabels_line, self.xlocs_line, self.ylabels_line,
            self.ylocs_line) = self.getEastingsNorthings(self.line_geotrans,
            self.cols_line, self.rows_line)
        # read in tifs from directory and adjust
        self.readTifs()

    def readTifs(self):
        """
        # read in farm space raster
        """
        # Read in Extent, kmap and farm Tiff and create 2-d array mask
        self.temp_kmap_extent = os.path.join(self.outputDataPath, 'temp_kmap_Extent.tif')
        self.extentTifFname = os.path.join(self.outputDataPath, 'tempExtent.tif')
        self.extMask = self.readTifIncreaseRes(self.extentTifFname)
        self.kmapExtent = self.readTifIncreaseRes(self.temp_kmap_extent)
        # modify extent mask
        self.extMask[self.kmapExtent < self.resultsdata.HabitatThreshold] = 0

    def makeFarmSeuTable(self):
        """
        make farm by year table of mean SeU
        """
        self.farmSeuTable = self.resultsdata.farmSeuTable
        nrow = np.shape(self.farmSeuTable)[0] 
        # calc mean across all 
        meanAcrossYears = np.mean(self.farmSeuTable[:-1, :-1], axis=1)
        self.farmSeuTable[:-1, -1] = np.round(meanAcrossYears, 2)
        self.farmSeuTable[-1] = np.round(np.mean(self.farmSeuTable[:-1], axis=0), 2)
        yrN = ['HerdID'] + list(self.yrNames) + ['MeanAllYears']
        aa = prettytable.PrettyTable(list(yrN))
        self.ownerNames = self.resultsdata.herdID.astype(str)
        self.farmRowNames = np.append(self.ownerNames, 'MeanAllFarms')
        self.farmSeuTable = np.round(self.farmSeuTable, 2)
        for i in range(nrow):
            name = self.farmRowNames[i]
            row = [name] + self.farmSeuTable[i].tolist()
            aa.add_row(row)
#        print('##############   Farm Seu Table    ###########')
#        print(aa)

    def writeFarmSeuToFile(self):
        """
        write text file to directory of seu means for every farm and year
        """
        (m, n) = self.farmSeuTable.shape
        # create new structured array with columns of different types
        mydtype = [('HerdID', 'U12')]  # 12 is the length of 'MeanAllYears' - the longest thing we will store
        yrNames = list(self.yrNames)
        fmts = ['%s']
        colNames = 'HerdID'
        for yr in yrNames:
            mydtype.append((yr, 'f8'))
            fmts.append('%.2f')
            colNames = colNames + ' ' + yr
        mydtype.append(('MeanAllYears', 'f8'))
        fmts.append('%.2f')
        colNames = colNames + ' MeanAllYears'
        self.farmSeuDat = np.zeros(m, dtype=mydtype)
        self.farmSeuDat['HerdID'] = self.farmRowNames
        for i in range(self.nYears):
            yr = yrNames[i]
            self.farmSeuDat[yr] = self.farmSeuTable[:, i]
        self.farmSeuDat['MeanAllYears'] = self.farmSeuTable[:, -1]
        farmSeuFname = os.path.join(self.outputDataPath, 'farmSeuTable.txt')
        np.savetxt(farmSeuFname, self.farmSeuDat, delimiter=',',
                fmt=fmts, header=colNames)

    def plotKmap(self):
        """
        plot of kmap with farm boundaries
        """
        # modify kmap based on extent mask
        kmap = self.kmapExtent * self.extMask
        kmap[kmap == 0] = -9999
        kmap_masked_array = np.ma.masked_where(kmap == -9999, kmap)
        cmap = P.cm.jet
        cmap.set_bad('w', 1.)
        P.figure(figsize=(11, 11))
        ax = P.gca()
        imgplot = ax.imshow(kmap_masked_array, cmap=cmap)
        maxKmap = np.max(kmap)
        # plot vcz boundary
        P.imshow(self.vczLineRaster)
        P.title('K-map; non-habitat removed', fontsize=10)
        P.xticks(self.xlocs_line, self.xlabels_line, fontsize=10)
        P.yticks(self.ylocs_line, self.ylabels_line, fontsize=10)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="2%", pad=0.04)
        cbar = P.colorbar(imgplot, cax=cax)
        imgplot.set_clim(0.0, maxKmap)
        cbar.ax.tick_params(labelsize=10)
        kmapFigFname = os.path.join(self.outputDataPath, 'kmap_vcz.png')
        P.savefig(kmapFigFname, format='png')
        P.show()

    def writeMeanSeuTif(self):
        """
        Write a tif for each year of meanSeu - for assessing in GIS
        """
        self.meanSeU = self.resultsdata.meanSeU
        # loop thru years
        for i in range(self.resultsdata.nyears):
            yr = self.resultsdata.years[0] + i
            meanSeU_i = self.meanSeU[i]
            name_i = 'meanSeU' + str(yr) + '.tif'
            tempTifName = os.path.join(self.outputDataPath, name_i)
            self.writeTmpTif(meanSeU_i, tempTifName, gdt_type = gdalconst.GDT_Float32)
        

class CombineData(object):
    def __init__(self, resultsdata, resultsprocess):
        """
        Object to combine wl and las spatial mean seu for each year
        Calc yearly SSe and PoF
        Because don't have iteration est of seu, only yearly means,
        we can only calculate mean SSe and PoF and cannot include RR.
        """
        self.resultsdata = resultsdata
        self.params = params
        self.resultsprocess = resultsprocess

        #########################
        # Run CombineData Functions
        # get spatial arrays
        self.seuLoopFX()
        # Calc SSe, PoF and EPI Combined
        self.calcSSePoFCombined()
        # Make spatial combined results table
        self.makeSpatialResultsTable()
        # write spatial combined results to directory
        self.writeSpatialCombined()

        # get wl summary of sse and pof from intermediate.txt
        self.calcWLSummary()

        # Optional functions
        self.plotOneYearCombinedSeu(params.imageYear)
        self.plotCombinedTemporal()

    #########################
    #   CombineData Class functions
    #########################

    def getFullSSeArrays(self, i):
        """
        get full LAS and WL arrays of SSe to resample if necessary
        """
        # get seuAve Las if year i is in las data
        if np.in1d(i, self.linkseqLAS):
            yrtemp = self.allYears[i]
            tid = self.las_yid[self.resultsprocess.years == yrtemp]
            seuAveLasTmp = self.resultsdata.seuAveLAS[:, tid]
            seuAveLasTmp = seuAveLasTmp.flatten()
            self.sseLASI = (1 - ((1 - seuAveLasTmp)**(self.resultsdata.pStarArray[i])))
        else:
            self.sseLASI = np.repeat(0, self.params.niter)
        # get seuAve WL if year i is in WL data
        if np.in1d(i, self.linkseqWL):
            yrtemp = self.allYears[i]
            wl_seuAveArray = self.wl_allSeUAve[self.wlYearArray == yrtemp]
            self.sseWLI = (1 - ((1 - wl_seuAveArray)**(self.resultsdata.pStarArray[i])))
        else:
            self.sseWLI = np.repeat(0.0, self.wl_iter)

    def sampleSSeLAS_WL(self, i):
        """
        get SSe if LAS have more iters than WL
        """
        # if more LAS than WL
        if self.params.niter > self.wl_iter:
            self.sseWLI = np.random.choice(self.sseWLI, size=self.params.niter, replace=True)
        # if more WL than LAS
        if self.params.niter < self.wl_iter:
            self.sseLASI = np.random.choice(self.sseLASI, size=self.wl_iter, replace=True)
        # combine LAS and WL
        self.sseTotal = 1 - ((1 - self.sseLASI) * (1 - self.sseWLI))


    def readInWLIntermediateData(self):
        """
        read in intermediate data: Sum(AR), ncells, and Ncells
        """
        intermediateFname = os.path.join(self.resultsprocess.tbpath, 'intermediateresults.txt')
        dnames = ('sumAR, meanseu, ncells, Ncells')
        self.intermediateData = np.genfromtxt(intermediateFname, names=dnames,
                delimiter=None,  dtype=['f8', 'f8', 'i8', 'i8'])
        self.wlSumAR = self.intermediateData['sumAR']
        self.wlMeanSeu = self.intermediateData['meanseu']
        self.wl_nCells = self.intermediateData['ncells']
        self.wl_NCells = self.intermediateData['Ncells']
        self.wl_prp = self.wl_nCells / self.wl_NCells
        self.wl_allSeUAve = self.wlMeanSeu * self.wl_prp
        # wl years
        self.wlNYears = (self.params.wildlifeYearRange[1] -
            self.params.wildlifeYearRange[0]) + 1
        self.wlYears = np.arange(self.params.wildlifeYearRange[0],
            (self.params.wildlifeYearRange[1] + 1))

        self.wl_iter = len(self.wlSumAR) / self.wlNYears
        self.wlYearArray = np.repeat(self.wlYears, self.wl_iter)
        self.wl_yid = np.arange(self.wlNYears)
        self.wl_yrIndx = np.repeat(self.wl_yid, self.wl_iter)

    def calcWLSummary(self):
        """
        use intermediate.txt data to get WL POF and SSe means for each year
        """
        # get appropriate p* - if it is growing
        wlYrMask = np.in1d(self.allYears, self.wlYears)
        wlYrIndx = self.allYrSeq[wlYrMask]
        wlPStar = self.resultsdata.pStarArray[wlYrIndx]
        wlPStarArray = (np.repeat(wlPStar, self.wl_iter)) / self.wl_NCells
        epiAveWl = wlPStarArray * self.wlSumAR / self.wl_nCells
        SSeWL = 1 - (1 - (self.wlMeanSeu * self.wl_prp))**(epiAveWl * self.wl_NCells)
        yrSeq = np.repeat(self.wl_yid, self.wl_iter)
        self.wlSSeByYear = np.zeros(self.wlNYears)
        self.wlPoFByYear = np.zeros(self.wlNYears)
        prior = self.params.prior[0]
        for i in range(self.wlNYears):
            self.wlSSeByYear[i] = np.mean(SSeWL[yrSeq == i])
            pof = prior / (1.0 - (self.wlSSeByYear[i] * (1.0 - prior)))
            self.wlPoFByYear[i] = pof
            prior = pof * (1.0 - 0.01)

    ###################################
    # Spatial calc of combined seu...
    ###################################

    def makeWL_SeuArray(self):
        """
        SPATIAL:  make 3-d array of mean seu for each year in Wildlife module
        """
        # read in wl intermediate data
        self.readInWLIntermediateData()
        # all years of data
        self.allYears = np.unique(np.append(self.resultsprocess.years, self.wlYears))
        self.nAllYears = len(self.allYears)
        self.allYrSeq = np.arange(self.nAllYears)
        self.allYearNames = self.allYears.astype('str')
        self.lasMask = np.in1d(self.allYears, self.resultsprocess.years)
        self.wlMask = np.in1d(self.allYears, self.wlYears)
        self.linkseqLAS = self.allYrSeq[self.lasMask]
        self.linkseqWL = self.allYrSeq[self.wlMask]
        self.las_yid = np.arange(self.resultsdata.nyears)
        # make template 3-d array to populate with mean seu values for each year
        self.wlSeuArray = np.zeros((self.wlNYears, self.resultsdata.rows,
            self.resultsdata.cols))
        # template array for adding wl and las
        self.combineSeuArray = np.zeros((self.nAllYears, self.resultsdata.rows,
            self.resultsdata.cols))

    def calcSSePoFCombined(self):
        """
        # get SeuAve and SSe for total years of wl and las
        """
        self.spatialPriorI = self.params.prior[0]
        self.spatialIntro = self.params.pIntro[0]
        self.EPIAveCombined = np.zeros(self.nAllYears)
        self.prpCombined = np.zeros(self.nAllYears)
        self.seuAveCombined = np.zeros(self.nAllYears)
        self.sseCombined = np.zeros(self.nAllYears)
        self.pofCombined = np.zeros(self.nAllYears)
        # loop thru years
        for i in range(self.nAllYears):
            seuArrayI = self.combineSeuArray[i] * self.resultsdata.extMask
            seuMaskI = seuArrayI > 0
            nSearchedI = np.sum(seuMaskI)
            self.prpCombined[i] = nSearchedI / self.resultsdata.Ntotal
            # do calculations to get EPIAve
            sumARi = np.sum(self.resultsdata.AR[seuMaskI])
            self.EPIAveCombined[i] = self.resultsdata.pStarPrp[i] * sumARi / nSearchedI
            # get seuAve for each year
            self.getSeuAveCombined(i, seuMaskI)
            # get SSe and PoF for year i
            self. SSePof_calc(i)

    def getSeuAveCombined(self, i, seuMaskI):
        """
        # get SSe for combined WL and LAS analysis
        """
        # get seuAve for each year
        seuCombinedI = self.combineSeuArray[i]
        seuAveI = np.mean(seuCombinedI[seuMaskI])
        self.seuAveCombined[i] = seuAveI
        # exponent term for SSe calc
        self.expTerm = self.EPIAveCombined[i] * self.resultsdata.Ntotal

    def SSePof_calc(self, i):
        """
        calc SSe and PoF for year i
        """
        SSePre = 1.0 - ((1.0 - self.seuAveCombined[i] * self.prpCombined[i])**self.expTerm)
        # mean system sensitivity in year i
        self.sseCombined[i] = SSePre
        pofI = self.spatialPriorI / (1.0 - (SSePre * (1.0 - self.spatialPriorI)))
        self.pofCombined[i] = pofI
        self.spatialPriorI = pofI * (1.0 - self.spatialIntro)

    def makeSpatialResultsTable(self):
        """
        populate the spatial combined results table and print to screen
        """
        self.spatialCombinedResultsTable = np.zeros(shape=(3, self.nAllYears))
        self.spatialCombinedResultsTable[0] = self.sseCombined
        self.spatialCombinedResultsTable[1] = self.pofCombined
        self.spatialCombinedResultsTable[2] = self.prpCombined
        self.spatialCombinedResultsTable = np.round(self.spatialCombinedResultsTable, 3)
        self.spatialCombinedResultsTable = self.spatialCombinedResultsTable.transpose()
        cc = prettytable.PrettyTable(['Years', 'Mean SSe', 'Mean PoF', 'Prop. Searched'])
        for i in range(self.nAllYears):
            name = self.allYearNames[i]
            row = [name] + self.spatialCombinedResultsTable[i].tolist()
            cc.add_row(row)
        print('###############  Combine Spatial Results Table   #############')
        print(cc)

    def writeSpatialCombined(self):
        """
        Write spatial combined results table to directory
        """
        (m, n) = self.spatialCombinedResultsTable.shape
        # create new structured array with columns of different types
        structured = np.empty((m,), dtype=[('Years', 'U12'), ('Mean SSe', np.float),
                    ('Mean PoF', np.float), ('Prop. Searched', np.float)])
        # copy data over
        structured['Mean SSe'] = self.spatialCombinedResultsTable[:, 0]
        structured['Mean PoF'] = self.spatialCombinedResultsTable[:, 1]
        structured['Prop. Searched'] = self.spatialCombinedResultsTable[:, 2]
        structured['Years'] = self.allYearNames
        wl_las_ResultsFname = os.path.join(self.resultsprocess.tbpath, 'WL_LAS_ResultsTable.txt')
        np.savetxt(wl_las_ResultsFname, structured, fmt=['%s', '%.4f', '%.4f', '%.4f'],
                   header='Years Mean_SSe Mean_PoF Prop_Searched')

    def plotOneYearCombinedSeu(self, yr):
        """
        make one image plot of combined meanSeu for a specified year
        """
        mask = self.allYears == yr
        self.yid = self.allYrSeq[mask]
        raster = self.combineSeuArray[self.yid]
        # do this to get rid of 3rd dimension
        raster = raster[0]
        raster[self.resultsdata.extMask == 0] = -9999
        # write tmp tif to change resolution
        self.tempSeuYearFname = os.path.join(self.resultsprocess.tbpath, 'temp_seu_year.tif')
        self.resultsprocess.writeTmpTif(raster, self.tempSeuYearFname, gdt_type = gdalconst.GDT_Float32)
        # tif back in with higher resolution
        raster = self.resultsprocess.readTifIncreaseRes(self.tempSeuYearFname)
        # set non-extent to white
        masked_array = np.ma.masked_where(raster == -9999, raster)
        cmap = P.cm.jet
        cmap.set_bad('w', 1.)
        P.figure(figsize=(11,9))
        ax = P.gca()
        imgplot = ax.imshow(masked_array, cmap=cmap)
        # plot the vczline raster array
        P.imshow(self.resultsprocess.vczLineRaster)
        maxSeu = np.max(raster)
        P.title('Mean WL & LAS SeU:  ' + str(yr), fontsize=10)
        P.xticks(self.resultsprocess.xlocs, self.resultsprocess.xlabels_string,
                 fontsize=10)
        P.yticks(self.resultsprocess.ylocs, self.resultsprocess.ylabels_string,
                 fontsize=10)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="2%", pad=0.04)
        cbar = P.colorbar(imgplot, cax=cax)
        imgplot.set_clim(0.0, maxSeu)
        cbar.ax.tick_params(labelsize=10)
        P.show()

    def getWLRaster(self, yr):
        """
        get wl seu raster for yr i
        """
        print('###### yr:', yr)
        # process wl seu raster
        tempFname = 'sensraster_' + str(yr) + '.asc'
        # path and file of wildlife seu *.asc rasters
        wlSeuAscii = os.path.join(self.resultsprocess.tbpath, tempFname)      # wl *.asc raster of seu for year i
        wlSeuTif = os.path.join(self.resultsprocess.tbpath, 'temp_wl_seu.tif')      # wl *.tif raster with correct extent and res
        # read in asc and  write temp tif for year i
        self.makeWLseuTif(wlSeuAscii, wlSeuTif)
        # read temp tif for year i
        self.wlSeuYrI = gdal.Open(wlSeuTif).ReadAsArray()

    def getLASRaster(self, yr):
        """
        get las seu raster
        """
        lasMask = self.resultsprocess.years == yr
        lasYid = self.resultsprocess.yseq[lasMask]
        self.lasArray = self.resultsdata.meanSeU[lasYid]
        self.lasArray = self.lasArray[0]

    def seuLoopFX(self):
        """
        SPATIAL: loop thru years in wl analysis
        """
        # make wl seu 3-d array to populate
        self.makeWL_SeuArray()
        # loop thru years
        for i in range(self.nAllYears):
            yr = self.allYears[i]
            # if have wl in yr i
            if self.wlMask[i]:
                # get wl seu raster
                self.getWLRaster(yr)
            else:
                # make wl array with zeros
                self.wlSeuYrI = np.zeros((self.resultsdata.rows,
                    self.resultsdata.cols))
            # if have las in year i
            if self.lasMask[i]:
                # process las seu raster
                self.getLASRaster(yr)
            else:
                self.lasArray = np.zeros((self.resultsdata.rows,
                    self.resultsdata.cols))
            # add the meanSeu from wl and las
            combineArray = 1.0 - ((1.0 - self.wlSeuYrI) * (1.0 - self.lasArray))
            self.combineSeuArray[i] = combineArray * self.resultsdata.extMask

    def makeWLseuTif(self, srcFname, dst_filename):
        """
        read in wl seu ascii, and write temp seu Tiff to directory
        """
        # Source - wl seu ascii; read in
        f_src = gdal.Open(srcFname, gdalconst.GA_ReadOnly)
        # write wl seu array to tif in directory
        dst = gdal.GetDriverByName('GTiff').Create(dst_filename, self.resultsdata.cols,
                    self.resultsdata.rows, 1, gdalconst.GDT_Float32)
        dst.SetGeoTransform(self.resultsdata.match_geotrans)
        dst.SetProjection(NZTM_WKT)
        # Reproject the kmap to the dimensions of the extent
        gdal.ReprojectImage(f_src, dst, NZTM_WKT, NZTM_WKT, gdalconst.GRA_Bilinear)
        del dst  # Flush
        del f_src

    def plotCombinedTemporal(self):
        """
        graph PoF and SSe results for: 1) LAS; 2) WL; and 3) combined
        """
        # graph PoF results.
        P.figure(figsize=(14, 6))
        P.subplot(1, 2, 1)

        P.plot(self.resultsprocess.years, self.resultsprocess.summaryTable[:, 0],
               label='Livestock', color='k', linewidth=5)
        P.plot(self.wlYears, self.wlPoFByYear,
               label='Wildlife', color='k', linewidth=5, ls='dashed')

        P.plot(self.allYears, self.pofCombined,
               label='Combined', color='r', linewidth=3)
        P.axhline(y=0.95, color='k', ls='dashed')
        minX = self.params.prior[0] - .05
        P.ylim([minX, 1])
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        self.resultsprocess.getNTicks()
        P.xticks(self.resultsprocess.xTicksYr, self.resultsprocess.xlabelsYr)
        P.xlabel('Years', fontsize=14)
        P.ylabel('Probability of eradication', fontsize=14)
        P.legend(loc='upper left')
        # graph SSe results
        P.subplot(1, 2, 2)
        P.plot(self.resultsprocess.years, self.resultsprocess.summaryTable[:, 3],
                label='Livestock', color='k', linewidth=5)
        P.plot(self.wlYears, self.wlSSeByYear,
                label='Wildlife', color='k', linewidth=5, ls='dashed')
        P.plot(self.allYears, self.sseCombined,
                label='Combined', color='r', linewidth=2)
        maxY = np.max(self.sseCombined) + .05
        P.ylim([0, maxY])
        ax = P.gca()
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(14)
        self.resultsprocess.getNTicks()
        P.xticks(self.resultsprocess.xTicksYr, self.resultsprocess.xlabelsYr)
        P.xlabel('Years', fontsize=14)
        P.ylabel('System sensitivity', fontsize=14)
        P.legend(loc='upper left')
        combinedSSePofFname = os.path.join(self.resultsprocess.tbpath, 'combinedSSePoF.png')
        P.savefig(combinedSSePofFname, format='png')
        P.show()
