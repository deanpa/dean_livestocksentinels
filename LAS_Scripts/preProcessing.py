#!/usr/bin/env python

import os
import numpy as np
import pickle
from osgeo import gdal
from osgeo import ogr
from osgeo import gdalconst
#import params
from numba import jit

# definition stolen from remote sensing files. Note this should be all one line in text editor.
NZTM_WKT = 'PROJCS["NZGD2000 / New Zealand Transverse Mercator 2000",GEOGCS["NZGD2000",DATUM["New_Zealand_Geodetic_Datum_2000",SPHEROID["GRS 1980",6378137,298.257222101,AUTHORITY["EPSG","7019"]],TOWGS84[0,0,0,0,0,0,0],AUTHORITY["EPSG","6167"]],PRIMEM["Greenwich",0,AUTHORITY["EPSG","8901"]],UNIT["degree",0.0174532925199433,AUTHORITY["EPSG","9122"]],AUTHORITY["EPSG","4167"]],PROJECTION["Transverse_Mercator"],PARAMETER["latitude_of_origin",0],PARAMETER["central_meridian",173],PARAMETER["scale_factor",0.9996],PARAMETER["false_easting",1600000],PARAMETER["false_northing",10000000],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AUTHORITY["EPSG","2193"]]'

# special codes for herd type
CODE_BB = 0     # Beef breeding
CODE_BD = 0     # Beef dry
CODE_DH = 0     # dairy herd
CODE_DD = 0     # dairy dry
CODE_S = 0      # replacement dairy
CODE_B = 1      # deer breeding
CODE_D = 1      # deer dry
CODE_V = 1      # deer velvet


########################################
#      Functions
########################################

def makeDistRaster(sig, res):
    """
    Pre calculate this so we don't have to do it each time
    """
    halfdist = 3.0 * sig
    halfcells = np.int(np.ceil(halfdist/res))
    winsize = (2 * halfcells) + 1
    halfwinsize = np.int(winsize / 2)
    distarray = np.empty((winsize, winsize))
    for x in range(winsize):
        for y in range(winsize):
            distx = (x - halfwinsize) * res
            disty = (y - halfwinsize) * res
            dist = np.sqrt(distx*distx + disty*disty)
            distarray[y, x] = dist
    return (distarray, winsize, halfwinsize)

@jit
def makePeripheralMask(periphMask, extentMask, farmMoveMask, nfarms, halfwinsize, nrows, ncols):
    """
    (1) return a 3-d mask by farm of peripheral cells to search
    # such as outside the farm boundary, or in forest patches on farm
    """
    ysize = nrows
    xsize = ncols
    for i in range(nfarms):
        for x in range(xsize):
            for y in range(ysize):
                if farmMoveMask[i, y, x] == 1:      # if cell is in farm i
                    # offset into dist array - deal with top and left edges
                    # top left x
                    tlx = x - halfwinsize
                    if tlx < 0:
                        tlx = 0
                    tly = y - halfwinsize
                    if tly < 0:
                        tly = 0
                    brx = x + halfwinsize
                    if brx > xsize - 1:
                        brx = xsize - 1
                    bry = y + halfwinsize
                    if bry > ysize - 1:
                        bry = ysize - 1
                    # print(x, y, tlx, brx, tly, bry)
                    for cx in range(tlx, brx+1):
                        for cy in range(tly, bry+1):
                            if (extentMask[cy, cx] == 1) and (farmMoveMask[i, cy, cx] == 0):   # if in extent but not where livstock move
                                periphMask[i, cy, cx] = 1
    return periphMask


@jit
def getDistDecayRast(extMask, farmMoveMask, periphFarmMask, distRaster, nrows,
                     ncols, nfarms, distDecayRaster, halfwinsize, maxDist):
    """
    # get distance-decay raster for all peripheral cells on all farms.
    # raster value is distance to nearest cell on farm i
    # make use of distRaster (the small window raster of distances)
    # Run one time up front
    """
    # loop thru cells
    for x in range(ncols):
        for y in range(nrows):
            # if in extent of VCZ
            if extMask[y, x] == 1:
                # loop thru farms
                for i in range(nfarms):
                    # if cell IS NOT accessible to animals on farm i
                    if periphFarmMask[i, y, x] == 1:
                        # minimum distance value, updated by target cells
                        minDist = maxDist
                        # offset into dist array - deal with top and left edges
                        xdistoff = 0
                        ydistoff = 0
                        # get coordinates of small distance block
                        # top left x
                        tlx = x - halfwinsize
                        if tlx < 0:
                            xdistoff = -tlx
                            tlx = 0
                        tly = y - halfwinsize
                        if tly < 0:
                            ydistoff = -tly
                            tly = 0
                        brx = x + halfwinsize
                        if brx > ncols - 1:
                            brx = ncols - 1
                        bry = y + halfwinsize
                        if bry > nrows - 1:
                            bry = nrows - 1
                        # cycly through
                        for cx in range(tlx, brx+1):
                            for cy in range(tly, bry+1):
                                # if target cell is accessible to animals on farm i
                                if (farmMoveMask[i, cy, cx] == 1):
                                    dist = distRaster[ydistoff + cy - tly, xdistoff + cx - tlx]
                                    if dist != 0.:
                                        if dist < minDist:
                                            minDist = dist
                                    distDecayRaster[i, y, x] = minDist

    return distDecayRaster


def getShapefileDimensions(layerFname, definition=False):
    """
    get x and y min and max from shapefile
    """
    dataset = ogr.Open(layerFname)
    layer = dataset.GetLayer()
    # print out definitions optional
    if definition:
        getShapeLayerDefinition(layer)
    # get dimensions
    xmin, xmax, ymin, ymax = layer.GetExtent()
    del dataset
    del layer
    return (xmin, xmax, ymin, ymax)

def getShapeLayerDefinition(layerName):
    """
    Get attribute definitions of shapefile using ogr
    Or, on command line, use ogrinfo <shapename.shp shapename>
    """
    layerDefinition = layerName.GetLayerDefn()
    print("Name  -  Type  Width  Precision")
    for i in range(layerDefinition.GetFieldCount()):
        fieldName = layerDefinition.GetFieldDefn(i).GetName()
        fieldTypeCode = layerDefinition.GetFieldDefn(i).GetType()
        fieldType = layerDefinition.GetFieldDefn(i).GetFieldTypeName(fieldTypeCode)
        fieldWidth = layerDefinition.GetFieldDefn(i).GetWidth()
        GetPrecision = layerDefinition.GetFieldDefn(i).GetPrecision()
        print(fieldName + " - " + fieldType + " " + str(fieldWidth) + " " + str(GetPrecision))

###########################################
# End Global functions
###########################################


class RawData():
    def __init__(self, params, kmapFname, extentFname, farmSpaceFname, testFname, 
            inputDataPath, outputDataPath):
        """
        Read in data and do one-time manipulations
        Pickle this class to directory
        """
        self.params = params
        self.inputDataPath = inputDataPath
        self.outputDataPath = outputDataPath
        ##########################################
        # RUN FUNCTIONS
        ##########################################
        self.makeMaskAndExtent(extentFname, farmSpaceFname)
        self.makeKmapTif(kmapFname)
        self.updateKmapAndExtent()
        self.readInTestingData(testFname)
        self.makeTestDatArray()                         # make 2-d array of testing data
        self.makeFarm3DRast()

#        self.make3D_ExtMask()

        (self.distRaster, self.winsize, self.halfwinsize) = makeDistRaster(self.params.sigma_mean[0], self.params.resol)

        # get 3-d mask of cells in periphery of each farm - use Numba
        self.wrapPeripFarmFX()

        self.wrapGetDistDecayRast()

        self.makePStarArray()

        self.getRR_Arrays()

        self.getPrpSearched()


    def wrapGetDistDecayRast(self):
        """
        call the numba function: getDistDecayRast()
        """
        self.distDecayRaster = np.zeros((self.nFarms, self.rows, self.cols))
        self.maxDist = np.max(self.distRaster)
        self.distDecayRaster = getDistDecayRast(self.extMask, self.farmMoveMask,
            self.periphFarmMask, self.distRaster, self.rows, self.cols, self.nFarms,
            self.distDecayRaster, self.halfwinsize, self.maxDist)
        self.distDecayRaster = (self.distDecayRaster**2) / 2

    def wrapPeripFarmFX(self):
        """
        function to call the Numba function: makePeripheralMask()
        """
        periphMask = np.zeros_like(self.farmMoveMask, dtype=int)
        self.periphFarmMask = makePeripheralMask(periphMask, self.extMask, self.farmMoveMask,
                self.nFarms, self.halfwinsize, self.rows, self.cols)

    def setGeoTrans(self, fs_xmin, fs_xmax, fs_ymin, fs_ymax, extent_xmin, extent_xmax,
                    extent_ymin, extent_ymax):
        """
        #### get dimensions that incorporate both extent shape and farm boundaries
        """
        self.xmin = np.min((fs_xmin, extent_xmin))
        self.xmax = np.max((fs_xmax, extent_xmax))
        self.ymin = np.min((fs_ymin, extent_ymin))
        self.ymax = np.max((fs_ymax, extent_ymax))
        self.cols = int((self.xmax - self.xmin) / self.params.resol)
        self.rows = int((self.ymax - self.ymin) / self.params.resol)
        self.match_geotrans = [self.xmin, self.params.resol, 0, self.ymax, 0,
                               -self.params.resol]

    def makeMaskAndExtent(self, ExtentShape, farmSpaceFname):
        """
        Use extent shapefile to make global mask
        """
        # get extent dimensions from farm boundaries and VCZ shapefile
        self.getDimensions(farmSpaceFname, ExtentShape)
        # create extent raster tiff
        dataset = ogr.Open(ExtentShape)
        extent_layer = dataset.GetLayer()
        # Extent tif name and directory
        self.extentTifFname = os.path.join(self.outputDataPath, 'tempExtent.tif')
        extent_ds = gdal.GetDriverByName('GTiff').Create(self.extentTifFname, self.cols,
                            self.rows, 1, gdal.GDT_Byte)
        extent_ds.SetGeoTransform(self.match_geotrans)
        extent_ds.SetProjection(NZTM_WKT)
        band = extent_ds.GetRasterBand(1)
        NoData_value = -9999
        band.SetNoDataValue(NoData_value)
        # Rasterize Extent and write to directory
        gdal.RasterizeLayer(extent_ds, [1], extent_layer, burn_values=[1])
        extent_ds.FlushCache()
        del dataset
        del extent_layer

    def getDimensions(self, farmSpaceFname, ExtentShape):
        """
        get extent dimensions from farm boundaries and VCZ shapefile
        """
        # read in farm boundary shapefile
        # make element of farmSpaceFname for use in postProcess
        self.farmSpaceFname = farmSpaceFname
        self.extentPolyFname = ExtentShape
        # get dimensions of farm boundaries
        (fs_xmin, fs_xmax, fs_ymin, fs_ymax) = getShapefileDimensions(farmSpaceFname,
                definition=False)
        # Get layer dimensions of extent shapefile
        (extent_xmin, extent_xmax, extent_ymin, extent_ymax) = getShapefileDimensions(
            ExtentShape, definition=False)
        # get dimensions that incorporate both extent shape and farm boundaries
        self.setGeoTrans(fs_xmin, fs_xmax, fs_ymin, fs_ymax, extent_xmin, extent_xmax,
                         extent_ymin, extent_ymax)
 
    def makeFarm3DRast(self):
        """
        iterate over herdid (#ownerID) to make each layer of 3-d farm, and movement mask
        """
        # read in farm_Space_Boundaries 
        self.farmDataSet = ogr.Open(self.farmSpaceFname)
        # integer mask where livestock can move
        self.farmMoveMask = np.zeros((self.nFarms, self.rows, self.cols), dtype=np.uint8)
        # integer mask where calc SeU
        self.farmExtentMask = np.zeros((self.nFarms, self.rows, self.cols), dtype=np.uint8)
        self.farmArea = np.empty(self.nFarms)
        self.possumOnFarmMask = np.zeros((self.nFarms, self.rows, self.cols), dtype=np.uint8)
        # loop thru herdID        
        self.makeHerdIDRastLayer()        

    def makeHerdIDRastLayer(self):
        """
        loop thru owner id, make temp tif and then raster layer of 3-d farm raster
        Note: for a given farm shape, HerdID varies but ownerID is constant 
        """
        # empty array to populate with herdIDs of farms that are too small for analysis
        self.tooSmallHerdID = np.array([])
        # farm space tif name and directory
        self.farmTifFname = os.path.join(self.outputDataPath, 'tempFarmSpace.tif')
        # loop thru herdId
        # use herdID because maintains flexibility to have different herd on diff blocks.
        for i in range(self.nFarms):
            oid = self.herdID[i]
            fs_layer = self.farmDataSet.GetLayer()
            fs_layer.SetAttributeFilter("HerdId = %d" % oid)
            # create destination farm space tif
            fs_ds = gdal.GetDriverByName('GTiff').Create(self.farmTifFname, self.cols,
                                                     self.rows, 1, gdal.GDT_UInt32)
            fs_ds.SetGeoTransform(self.match_geotrans)
            fs_ds.SetProjection(NZTM_WKT)
            band = fs_ds.GetRasterBand(1)
            NoData_value = 0
            band.SetNoDataValue(NoData_value)
            # Rasterize FarmSpace and write to directory
            gdal.RasterizeLayer(fs_ds, [1], fs_layer, burn_values=[1])
            fs_ds.FlushCache()
            del fs_layer
            del fs_ds
            # read in temp tif for each farm and fill in the ownerID layer of 3D raster
            self.populate3DFarmRast(i)
        # remove farms to small or narrow to be analysed
        self.cullTooSmallFarms()
        del self.farmDataSet


    def cullTooSmallFarms(self):
        """
        remove from 3-d rasters and data arrays the data from small farms
        """
        # mask of herdID (farms) that are large enough to be analysed
        self.maskKeepLargeFarms = np.in1d(self.herdID, self.tooSmallHerdID, invert = True)
        self.herdID = self.herdID[self.maskKeepLargeFarms]
###        self.ownerID = self.ownerID[self.maskKeepLargeFarms]
#        self.uOwnerID = np.unique(self.ownerID)
#        self.nOwnerID = len(self.uOwnerID)
        self.herdType = self.herdType[self.maskKeepLargeFarms]
        self.sentinelType = self.sentinelType[self.maskKeepLargeFarms]
        self.testDat = self.testDat[self.maskKeepLargeFarms]
        self.nTestByYear = self.nTestByYear[self.maskKeepLargeFarms]
        self.farmArea = self.farmArea[self.maskKeepLargeFarms]
        self.farmExtentMask = self.farmExtentMask[self.maskKeepLargeFarms]
        self.possumOnFarmMask = self.possumOnFarmMask[self.maskKeepLargeFarms]
        self.farmMoveMask = self.farmMoveMask[self.maskKeepLargeFarms]
        self.nFarms = len(self.herdID)

    def populate3DFarmRast(self, i):
        """
        read in temp tif for each farm and fill in the ownerID layer of 3D raster
        """
        # read in temp farm tif for ownerId i (zero and one values)
        farmSpaceRast = gdal.Open(self.farmTifFname).ReadAsArray()
        # entire farm, including area outside VCZ
        tmpMask = farmSpaceRast.copy()
        # where animals can go, including area outside VCZ
        tmpMask2 = tmpMask * self.GoMask
        if np.sum(tmpMask) < 1.0:
            # add to list of too small farms (herdID)
            self.tooSmallHerdID = np.append(self.tooSmallHerdID, self.herdID[i])
        # 3-d array where livestock can go, including outside VCZ
        self.farmMoveMask[i] = tmpMask2
        # where animals can go and possums can live ("farm within VCZ extent")
        tmpMask3 = tmpMask2 * self.extMask
        # total area in hectares over which livestock can move, including outside vcz
        self.farmArea[i] = (np.sum(tmpMask2)) * ((self.params.resol**2.0) / 10000.0)
        # Mask of where livestock can go on farm and within the VCZ that possums
        # can have homerange centre. Used to calc proportion area searched
        self.farmExtentMask[i] = tmpMask3
        # Mask of where possum have HR centre on farm
        self.possumOnFarmMask[i] = tmpMask * self.extMask

    def makeKmapTif(self, kmapFname):
        """
        read in kmap ascii, and write kmap Tiff to directory
        """
        # Source - Kmap
        kmap_src = gdal.Open(kmapFname, gdalconst.GA_ReadOnly)
        # write kmap array to tif in directory
        # temp kmap tif name and directory
        self.temp_kmap_extent = os.path.join(self.outputDataPath, 'temp_kmap_Extent.tif')
        dst = gdal.GetDriverByName('GTiff').Create(self.temp_kmap_extent, self.cols, self.rows,
                            1, gdalconst.GDT_Float32)
        dst.SetGeoTransform(self.match_geotrans)
        dst.SetProjection(NZTM_WKT)
        # Reproject the kmap to the dimensions of the extent
        gdal.ReprojectImage(kmap_src, dst, NZTM_WKT, NZTM_WKT, gdalconst.GRA_Bilinear)
        del dst  # Flush
        del kmap_src

    def updateKmapAndExtent(self):
        """
        1) make extentMask values = 0 where kmap is below habitat threshold
        2) make kmap values = 0 outside mask
        """
        # Read in Extent, kmap and farm Tiff and create 2-d array mask
        self.extMask = gdal.Open(self.extentTifFname).ReadAsArray()
        # self.farmSpaceRast = gdal.Open(self.farmTifFname).ReadAsArray()
        self.kmapFull = gdal.Open(self.temp_kmap_extent).ReadAsArray()
        # modify extent mask and kmapExtent
        self.extMask[self.kmapFull < self.params.HabitatThreshold] = 0
        self.kmapExtent = self.kmapFull * self.extMask
        self.extentMaskBool = self.extMask == 1
        # make go layer to update farm mask
        self.GoMask = np.zeros((self.rows, self.cols), dtype=np.uint8)
        self.GoMask[self.kmapFull < self.params.noGoThreshold] = 1
        # total number of cells that could have TB
        self.Ntotal = np.sum(self.extMask)


    def readInTestingData(self, testFname):
        """
        read in testing data
        """
        self.nyears = (self.params.years[1] - self.params.years[0]) + 1
        self.nEntries = self.nyears * 3
        surveyDataTypes = np.repeat('i8', self.nEntries)
        farmData = np.array(['i8', 'S10', 'i8', 'i8'])
        datatypes = np.append(farmData, surveyDataTypes)
        self.test = np.genfromtxt(testFname,  delimiter=',', names=True, dtype=datatypes)
        # specify arrays
        self.herdID = self.test['HerdID']
        self.herdType = self.test['HerdType']
###        self.ownerID = self.test['OwnerID']
        self.nFarms = len(self.herdID)
###        self.nFarms = len(self.ownerID)
        self.sentinelType = np.ones(self.nFarms, dtype=int)
        self.sentinelType[self.herdType == b'BB'] = CODE_BB     # Beef breeding
        self.sentinelType[self.herdType == b'BD'] = CODE_BD     # Beef Dry
        self.sentinelType[self.herdType == b'DH'] = CODE_DH     # Dairy herd
        self.sentinelType[self.herdType == b'DD'] = CODE_DD     # Dairy Dry
        self.sentinelType[self.herdType == b'S'] = CODE_S       # replacement Dairy
        self.sentinelType[self.herdType == b'B'] = CODE_B       # Deer Breeding
        self.sentinelType[self.herdType == b'D'] = CODE_D       # Deer Dry
        self.sentinelType[self.herdType == b'V'] = CODE_V       # deer velvet


    def makeTestDatArray(self):
        """
        make 2-d array of testing data for years of interest
        Run 1 time, up front
        """
        nDatColumns = self.nyears * 2
        self.testDat = np.zeros((self.nFarms, nDatColumns))
        self.years = np.arange(self.nyears)
        self.testtype = np.arange(2)            # field testing and abattoir
        testNames = np.array(['Tested_', 'Abattoir_'])
        self.nTestByYear = np.zeros((self.nFarms, self.nyears))
        # cycle through years of testing data (only those of interest)
        for i in self.years:
            # cycle through test types
            for j in self.testtype:
                # get column name in self.test
                columnName = testNames[j]+str(i)
                tmpDat = self.test[columnName]
                colCount = (i * 2) + j
                self.testDat[:, colCount] = tmpDat
                # get number of total tests on each farm and year
                self.nTestByYear[:, i] += tmpDat
        self.cullNoTestHerds()

    def cullNoTestHerds(self):
        """
        remove herds without any TB testing data
        """
        # make mask to keep herds with tests
        sumTestsByHerd = np.sum(self.testDat, axis = 1)
        self.maskHerdsWithTests = sumTestsByHerd > 0
        # update data arrays by culling out no-test herds
        self.testDat = self.testDat[self.maskHerdsWithTests]
        self.nTestByYear = self.nTestByYear[self.maskHerdsWithTests]
        self.herdID = self.herdID[self.maskHerdsWithTests]
        self.herdType = self.herdType[self.maskHerdsWithTests]
###        self.ownerID = self.ownerID[self.maskHerdsWithTests]
        self.nFarms = len(self.herdID)
###        self.nFarms = len(self.ownerID)
        self.sentinelType = self.sentinelType[self.maskHerdsWithTests]

    def getPrpSearched(self):
        """
        Calc proportion of cells in extmask that were searched each year
        """
        self.prpSearched = np.zeros(self.nyears)
        self.nSearched = np.zeros(self.nyears)
        for i in range(self.nyears):
            countRast = np.zeros((self.rows, self.cols))
            # loop thru farms
            for j in range(self.nFarms):
                if self.nTestByYear[j, i] > 0:
                    fmask = self.farmExtentMask[j]
                    f_periphMask = self.periphFarmMask[j]
                    countRast = countRast + fmask + f_periphMask
            countMask = countRast > 0
            self.nSearched[i] = np.sum(countMask)
            self.prpSearched[i] = self.nSearched[i] / self.Ntotal
            # do calculations to get EPIAve
            sumARi = np.sum(self.AR[countMask])
            self.EPIAve[i] = self.pStarPrp[i] * sumARi / self.nSearched[i]
###            print('nsearched i', i, self.nSearched[i], 'tot risk cells', self.Ntotal)


    def makePStarArray(self):
        """
        # make temporal array of PStar, allowing for increase over time
        # default is no change
        """
        yearArray = np.arange(self.nyears, dtype=float)
        increaseArray = yearArray * self.params.annual_PStar_Increase
        self.pStarArray = self.params.PStar + increaseArray
        self.pStarPrp = self.pStarArray / self.Ntotal

    def getRR_Arrays(self):
        """
        get info for calc rr and EPIAve
        """
        self.RR = (self.kmapExtent + 1.0) * self.extMask
        minKmap = np.min(self.RR[self.extMask == 1])
        self.RR = self.RR / minKmap
        self.sumAllRR = np.sum(self.RR)
        self.AR = self.Ntotal * self.RR / self.sumAllRR
        # populate epi in 'getPrpSearch' function
        self.EPIAve = np.zeros(self.nyears)

    #############################
    # End rawdata functions
    #############################
