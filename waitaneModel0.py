#!/usr/bin/env python

########################################
########################################
# This file is part of Proof of Absence
# Copyright (C) 2019 Dean Anderson and Sam Gillingham
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
########################################
########################################

import os
import pickle
import numpy as np

from LAS_Scripts import preProcessing
from LAS_Scripts import params
from LAS_Scripts import calculation

######################
# Main function
def main():
    ############################################################
    ###################################### USER MODIFY HERE ONLY
    #################################
    # set paths to scripts and data
    inputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 'LAS', 
            'Waitane', 'Data')
    outputDataPath = os.path.join(os.getenv('POFPROJDIR', default = '.'), 'LAS', 
            'Waitane', 'Results', 'Model0')

    # Kmap ascii
    kmapDatFile = os.path.join(inputDataPath, 'RR_waitane.tif')   
    # Extent shapefile    
    extentDatFile = os.path.join(inputDataPath, 'waitaneBoundary.shp')    
    # Farm boundary shapefile
    farmSpaceDatFile = os.path.join(inputDataPath, 'waitaneFarmBoundaries.shp')  
    # Herd testing data
    testDatFile = os.path.join(inputDataPath, 'waitane_testdata.csv')     

   ############ IF FIRST RUN CONDITION
    # if True, do preprocessing, else skip to calculations
    firstRun = True        # True or False

    # Instance of POAParameters class
    myParams = params.LASParameters()

    ## NUMBER OF ITERATIONS
    myParams.setNumIterations(10)
    ## YEARS
    myParams.setYears(np.array([2008, 2017]))
    print('Years', myParams.years)
    ## PRIOR
    myParams.setPrior(np.array([.85, .02]))
    print('prior mean and sd', myParams.prior)
    # probability of Introduction - mean and sd
    myParams.setPIntro(np.array([.001, .0001]))
    # resolution of pixels
    myParams.setResol(100)
    # kmap threshold
    myParams.setHabitatThreshold(10)
    print('Habitat Threshold', myParams.HabitatThreshold)
    # habitat threshold above which livestock cannot access
    myParams.setNoGoThreshold(80)
    # Design prevalence expressed as number of infected cells
    myParams.setPStar(2)
    print('PStar', myParams.PStar)
    # annual rate of increase in design prevalence; default = 0
    myParams.setAnnual_PStar_Increase(0.0)     # 1.0  # .25
    print('Increase rate of PStar', myParams.annual_PStar_Increase)
    # maximum p(TB transmission from infected possum)
    myParams.setImax(np.array([0.1, .01]))
    # sigma mean and sd
    myParams.setSigma_mean(np.array([90.0, 10.0]))
    print('sigma mean and sd', myParams.sigma_mean)

    ###############
    # Testing Sensitivities
    myParams.setCattleFieldTest(np.array([[0.83, .08],    # cft
                                     [0.93, 0.03],   # bovigam
                                     [0.65, 0.05],   # LSNC
                                     [0.98, 0.02]]))  # culture
    myParams.setCattleAbattoir(np.array([[0.50, 0.05],    # LSNC-ab
                                    [0.95, 0.05],    # Histopathology
                                    [0.98, 0.02]]))   # culture
    myParams.setDeerFieldTest(np.array([[0.7, .10],       # CCT
                                   [0.93, 0.03],     # ELISA
                                   [0.65, 0.05],     # LSND
                                   [0.98, 0.02]]))    # culture
    myParams.setDeerAbattoir(np.array([[0.45, 0.1],       # LSND-ab
                                  [0.95, 0.05],      # Histopathology
                                  [0.98, 0.02]]))     # culture
    ###################################################

    print('firstRun = ', firstRun)
    print('Number of Iterations', myParams.niter)

    if firstRun:
        # initiate instances of Classes
        rawdata = preProcessing.RawData(myParams, kmapDatFile, extentDatFile, 
            farmSpaceDatFile, testDatFile, inputDataPath, outputDataPath)

        # pickle to output directory
        outRawData = os.path.join(outputDataPath, 'spatialData.pkl')
        fileobj = open(outRawData, 'wb')
        pickle.dump(rawdata, fileobj, protocol=4)
        fileobj.close()

    # If preProcessing has already been run (not first run)
    else:
        # unpickle results from preProcessing.py
        PKLFName = os.path.join(outputDataPath, 'spatialData.pkl')
        # unpickle preprocessing
        fileobj = open(PKLFName, 'rb')
        rawdata = pickle.load(fileobj)
        fileobj.close()


    ## RUN calculation.py to get sse and pof
    processdata = calculation.ProcessData(myParams, rawdata)
    ## MAKE CLASS OBJECT OF ELEMENTS FOR PICKLING
    resultsdata = calculation.ResultsData(rawdata, processdata, myParams)
    # pickle results for post processing in postProcessing.py
    outResults = os.path.join(outputDataPath, 'out_results.pkl')
    fileobj = open(outResults, 'wb')
    pickle.dump(resultsdata, fileobj)
    fileobj.close()

 
if __name__ == '__main__':
    main()



