#!/usr/bin/env python

import numpy as np
import configparser


class Params(object):
    def __init__(self):
        """
        Object to set parameters
        """
        ###################################################
        #       USER
        ###################################################
        # Set number of  iterations
        self.niter = 20
        # Years of data
        self.startYear = 2008
        self.endYear = 2017
        # prior
        self.prior = np.array([.9, .04])
        # probability of Introduction - mean and sd
        self.pIntro = np.array([.001, .0001])
        # resolution of pixels
        self.resol = 100
        # kmap threshold
        self.HabitatThreshold = 10
        # habitat threshold above which livestock cannot access
        self.noGoThreshold = 80
        # Design prevalence expressed as number of infected cells
        self.PStar = 2
        # annual rate of increase in design prevalence; default = 0
        self.annual_PStar_Increase = 0.0     # 1.0  # .25
        # maximum p(TB transmission from infected possum)
        self.Imax = np.array([0.1, .01])
        # sigma mean and sd
        self.sigma_mean = np.array([90.0, 10.0])

        ###############
        # Testing Sensitivities
        self.cattleFieldTest = np.array([[0.83, .08],    # cft
                                         [0.93, 0.03],   # bovigam
                                         [0.65, 0.05],   # LSNC
                                         [0.98, 0.02]])  # culture
        self.cattleAbattoir = np.array([[0.50, 0.05],    # LSNC-ab
                                        [0.95, 0.05],    # Histopathology
                                        [0.98, 0.02]])   # culture
        self.deerFieldTest = np.array([[0.7, .10],       # CCT
                                       [0.93, 0.03],     # ELISA
                                       [0.65, 0.05],     # LSND
                                       [0.98, 0.02]])    # culture
        self.deerAbattoir = np.array([[0.45, 0.1],       # LSND-ab
                                      [0.95, 0.05],      # Histopathology
                                      [0.98, 0.02]])     # culture
        ###################################################

        ##################################################
        #  Processing params
        ##################################################
        self.combineWLResults = False
        self.imageYear = 2010
        self.wildlifeYearRange = np.array([2006, 2009])
        self.files = {}

    def load(self, filename):
        config = configparser.ConfigParser()
        config.read(filename)
        if 'sentinels' not in config:
            return
        self.niter = int(config['sentinels']['iterations'])
        self.imageYear = int(config['sentinels']['imageYear'])
        self.startYear = int(config['sentinels']['startYear'])
        self.endYear = int(config['sentinels']['endYear'])
        self.HabitatThreshold = int(config['sentinels']['habitat'])
        self.noGoThreshold = int(config['sentinels']['noGo'])
        self.PStar = float(config['sentinels']['pstar'])
        self.annual_PStar_Increase = float(config['sentinels']['pstarAnnual'])
        self.resol = int(config['sentinels']['resolution'])

        # get the data filenames
        self.files["kmap"] = config['sentinels']['kmap']
        self.files["region"] = config['sentinels']['region']
        self.files["farms"] = config['sentinels']['farms']
        self.files["disease"] = config['sentinels']['disease']
