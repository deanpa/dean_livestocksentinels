#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
import pickle


def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

def orProbability(xx):
    return 1 - np.prod(1 - xx)

# special codes for herd type
CODE_Deer = 1

 
class Params():
    def __init__(self):
        """
        create some params to use throughout
        """
        ###########################################
        self.infected = 10 # 5   # np.arange(1,200       # possum design prevalence
        self.sigma = 200
        self.I0 = .25
        self.pof = 0.5
        ###########################################
        self.nyears = 13
        # field tests of deer
        self.dftest = np.array([0.7, .93, .65, .98])
        self.deerfieldSe = np.prod(self.dftest)
        # deer abattoir tests
        self.datest = np.array([0.45, .95, 0.98])
        self.deerabbSe = np.prod(self.datest)

        # field tests of cattle
        self.cftest = np.array([0.83, .93, .65, .98])
        self.cattlefieldSe = np.prod(self.cftest)
        # abattoir tests of cattle
        self.catest = np.array([0.5, .95, 0.98])
        self.cattleabbSe = np.prod(self.catest)
        # array of test se for cattle and deer in field and abattoir
        self.testSE = np.array([self.cattlefieldSe, self.cattleabbSe, self.deerfieldSe, self.deerabbSe])


class RunData(object):
    def __init__(self, params, livestockFname, habitatFname):
        """
        Object to read in livestock and habitat data
        """
        self.params = params
        # habitat data
        self.habitatDat = np.genfromtxt(habitatFname, delimiter=',', names=True,
            dtype=['i8', 'i8', 'f8', 'f8'])

        self.kval = self.habitatDat['kval']
        self.habX = self.habitatDat['x']
        self.habY = self.habitatDat['y']

        # livestock data
        self.livestockDat = np.genfromtxt(livestockFname, delimiter=',', names=True,
            dtype=['i8', 'S10', 'f8', 'f8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8',
                    'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8',
                    'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8', 'i8',
                    'i8', 'i8', 'i8', 'i8'])
        self.hid = self.livestockDat['HerdID']
        self.type = self.livestockDat['HerdType']
        self.herdX = self.livestockDat['Long']
        self.herdY = self.livestockDat['Lat']
        self.nherds = len(self.herdX)
        self.testdat = np.zeros((self.nherds,(self.params.nyears * 3)), dtype = int)

        # convert string herd types to integers
        self.htype = np.zeros(self.nherds, dtype = int)
        self.htype[self.type == b'B'] = CODE_Deer
        self.htype[self.type == b'D'] = CODE_Deer

        ###################################
        ## Run functions
        ####################################
        # run fx to populate test data array
        self.populateTestDat()
        # get mean x and y
        self.getMeanHabXY()
        # indx of herds tested in each year and sampling method
        self.makeMethodIndx()
        # get herd distance
        self.getHerdDist()
        # ptrans
        self.getPTransmission()
        # epi
        self.getEPI()
        # get test se 2-d array
        self.getTestSeArray()
        # get seUAve for all herds
        self.getSeUAve()
        # get seH across herds and years
        self.getSeH()

        ##########################
        ##########################
        # get SSe using real data in which only subset were surveyed
#        self.getSSe()
        #Get SSe if all herds surveyed
        self.getMaxHerdSe()
        ##########################
        #########################


    ##############################
    ##  Functions
    ##############################
    def populateTestDat(self):
        """
        populate test data array
        """
        self.yearIndx = np.arange(39).reshape(13,3)
        tmpArr = np.zeros((self.nherds, 3), dtype = int)
        for i in range(self.params.nyears):
            tmpArr[:,0] = self.livestockDat[('HerdSize_'+str(i))]
            tmpArr[:,1] = self.livestockDat[('Tested_'+str(i))]
            tmpArr[:,2] = self.livestockDat[('Abattoir_'+str(i))]
            self.testdat[:, self.yearIndx[i]] = tmpArr
            
    def getMeanHabXY(self):
        """
        get weighted mean habitat location
        """
        xtmp = self.habX[self.kval > 0]
        ytmp = self.habY[self.kval > 0]
        ktmp = self.kval[self.kval > 0]
        sumWKX = np.sum(xtmp * ktmp)
        sumW = np.sum(ktmp)
        self.meanX = sumWKX / sumW
        sumWKY = np.sum(ytmp * ktmp)
        self.meanY = sumWKY / sumW
        ncells = len(ktmp)/4
        print('ncells', ncells)

    def makeMethodIndx(self):
        """
        make 2-d array of index of sampling method (binomial or hypergeo)
        """
        self.methodIndx = np.zeros((self.nherds, self.params.nyears), dtype = int)
        for i in range(self.params.nyears):
            for j in range(self.nherds):
                tmpdat = self.testdat[j, self.yearIndx[i]]
                cond1 = np.sum(tmpdat[1:2])
                if cond1 > 0:
                    # at least binomial and hypergeo if meets condition below
                    self.methodIndx[j,i] = 1
                    if tmpdat[0] > 0:
                        #hypergeometric
                        self.methodIndx[j,i] = 2

    def getHerdDist(self):
        """
        get mean dist from herds to wildlife risk
        """
        self.herdDist = distxy(self.herdX, self.herdY, self.meanX, self.meanY)        
#        print('herd dist', self.herdDist)
#        print('mean x y', self.meanX, self.meanY)
#        print('summary dist', np.mean(self.herdDist), np.min(self.herdDist), np.max(self.herdDist))

    def getPTransmission(self):
        """
        p(trans) given distance and I0
        """
        self.ptrans = self.params.I0 * np.exp(-(self.herdDist**2) / 2 / self.params.sigma**2)
#        print('ptrans', self.ptrans)


    def getEPI(self):
        """
        get average epi for each herd that was tested
        """
        self.epi = np.ones((self.nherds, self.params.nyears))
        epitmp = 1 - (1 - self.ptrans)**self.params.infected
        epitmp = np.expand_dims(epitmp, 1)
        self.epi = epitmp * self.epi
        self.epi[self.methodIndx == 0] = 0

    def getTestSeArray(self):
        """
        get test parameters for cattle or deer, and test or abattoir in 2-d array
        """
        cattlesetmp = self.params.testSE[0:2]       #.reshape(1,4)
        deersetmp = self.params.testSE[2:4]
        self.testSeArray = np.zeros((self.nherds, 2))
        self.testSeArray[self.htype == 0] = cattlesetmp    #testsetmp[0, 0:2]    #self.params.testSE[0:2]
        self.testSeArray[self.htype == 1] = deersetmp   #testsetmp[0, 2:4]    #self.params.testSE[2:4]
        

    def getSeUAve(self):
        """
        get SeU average for all herds over time
        """
        self.seUAve = np.zeros((self.nherds, self.params.nyears))
        ## number in herd - if number given
        self.herdSize = np.zeros((self.nherds, self.params.nyears))
        # total number tested by herd
        self.nTestsHerd = np.zeros((self.nherds, self.params.nyears))
        for i in range(self.params.nyears):
            seUAvetmp = np.zeros(self.nherds)
            yearIndxtmp = self.yearIndx[i,1:]
            herdIndxTmp = self.yearIndx[i, 0]
            self.herdSize[:, i] = self.testdat[:, herdIndxTmp]
            # number of tests and abattoir in appropriate year for all herds
            testdatTmp = self.testdat[:, yearIndxtmp]
            # total n tests by herd
            self.nTestsHerd[:, i] = np.sum(testdatTmp, axis = 1)
            calcMask = self.nTestsHerd[:, i] > 0
            # multiply n tests * the appropriate Se
            seSumtmp = testdatTmp * self.testSeArray
            seAvetmp = (np.sum(seSumtmp, axis = 1)[calcMask]) / self.nTestsHerd[calcMask, i]
            self.seUAve[calcMask, i] = seAvetmp 


    def getSeH(self):
        """
        get seH for herds across years
        """
        self.seH = np.zeros((self.nherds, self.params.nyears))
        # masks of method types
        HyperGeoMask = self.methodIndx == 2
        BinomMask = self.methodIndx == 1
        # get seH for binomial instances
        epiBin = self.epi[BinomMask]
        seUAveBin = self.seUAve[BinomMask]
        nTestedBin = self.nTestsHerd[BinomMask]
        seHBin = 1 - (1 - (epiBin * seUAveBin))**nTestedBin
        self.seH[BinomMask] = seHBin
        # get seH for HyperGeometric instances
        epiHyper = self.epi[HyperGeoMask]
        seUAveHyper = self.seUAve[HyperGeoMask]
        nTestedHyper = self.nTestsHerd[HyperGeoMask]
        herdSizeHyper = self.herdSize[HyperGeoMask]
        seHHyper = 1 - (1 - (seUAveHyper * nTestedHyper / herdSizeHyper))**(epiHyper * herdSizeHyper)
        self.seH[HyperGeoMask] = seHHyper
#        print('seH', self.seH[:,5:10])

    #############################################
    #############################################
    ##
    ##      These two functions can't be run together
    ##
    def getMaxHerdSe(self):
        """
        get max herd Se for each herd
        Use for calc in pre-eradication phase - Stage I
        All herds would be tested.
        """
        self.maxSeHerd = np.max(self.seH, axis = 1)
        self.SSeAllSurveyed = orProbability(self.maxSeHerd)
        print('maxSeHerd', self.maxSeHerd)
        print('self.SSeAllSurveyed', 1 - np.prod(1 - self.maxSeHerd))
        print('self.SSeAllSurveyed', self.SSeAllSurveyed)
        print('self.maxSeHerd greater 0', self.maxSeHerd[self.maxSeHerd > 0])




    def getSSe(self):
        """
        Calc SSe for entire area for each year
        """
        self.SSe = np.apply_along_axis(orProbability, 0, self.seH)
        print('SSe', self.SSe)
    ################################################
    ################################################


########            Main function
#######
def main():

    #livestockpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
    habitatDatFile = 'habData2.csv' # os.path.join(livestockpath,'habData2.csv')
    livestockDatFile = 'livestocktestdata2.csv' #os.path.join(livestockpath,'livestocktestdata2.csv')

    params = Params()

    rundata = RunData(params, livestockDatFile, habitatDatFile)

if __name__ == '__main__':
    main()
