#!/usr/bin/env python

import numpy as np

area = np.array([1, 5, 10, 25, 50, 100, 250, 500, 1000, 2500])
ntest = area * 2.5        # assuming 3 animals per ha
iMax = np.arange(.05, .5, .05)

######## DEER TRIAL
area = np.array([200, 400, 600])
ntest = area * .06        # assuming 3 animals per ha
iMax = np.arange(.05, .9, .05)
################################

print('iMax', iMax)

SSe = np.zeros((len(iMax), len(area)))

for i in range(len(area)):
    pInf = iMax / area[i]
    for j in range(len(iMax)):
        seu = 1 - (1 - pInf[j])**(ntest[i])
        SSe[j, i] = np.round(seu, 3)


print('SSe', SSe)









