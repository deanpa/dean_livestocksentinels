#!/usr/bin/env python

import liveStockSentinel
import params
import argparse
import os


class CmdArgs(object):
    def __init__(self):
        p = argparse.ArgumentParser()
        p.add_argument("--rawdata", help="Use raw dataset from a previous run")
        p.add_argument("--testdata", help="Use the specified test dataset", 
            choices=['blythe', 'buller', 'lees', 'nullherd', 'ash', 'benbolt', 'glencoe',
                    'repeater', 'retreat', 'valley', 'waitane'])
        p.add_argument("--settings", help="Use the simulation parameters found in the settings file")
        p.add_argument('--version', action='version', version='%(prog)s 1.0')
        args = p.parse_args()
        self.__dict__.update(args.__dict__)

cmdargs = CmdArgs()
if cmdargs.rawdata:
    print(cmdargs.rawdata)
if cmdargs.testdata:
    print(cmdargs.testdata)

simulation_parameters = params.Params()
if cmdargs.settings:
    tbpath = os.getenv('POFPROJDIR', default='.')
    simulation_parameters.load(os.path.join(tbpath, cmdargs.settings))

liveStockSentinel.main(simulation_parameters, cmdargs.rawdata, cmdargs.testdata)
