#!/usr/bin/env python

import os
from scipy import stats
import numpy as np
import pickle
import pylab as P

def distxy(x1,y1,x2,y2):
    return np.sqrt(np.power(x1 - x2, 2) + np.power(y1 - y2, 2))

def orProbability(xx):
    return 1 - np.prod(1 - xx)

# special codes for herd type
CODE_Deer = 1


class SpatialRun():
    def __init__(self):
        """
        create some params to use throughout
        """
        ###########################################
        self.infected = np.arange(1,21)  # 5   # np.arange(1,200       # possum design prevalence
        self.lenInfected = len(self.infected)

        print('infected',self.infected)
#        self.sigma = 200
        self.I0 = np.array([.01, .10, .25])
        self.nI0 = len(self.I0)
        self.N = 13000
        self.farmCells = 200
        self.nFarms = 32
        self.percentNonHab = 0.5
        self.seTest = 0.45
        self.n = self.nFarms * self.farmCells * self.percentNonHab
        self.prp = self.n / self.N
        print('prp', self.prp)
        self.pof = 0.5
        ##########################################
        ### RUN FUNCTIONS

        self.spatialCalc()
        self.plotFX()
        ###########################################
    def spatialCalc(self):
        """
        calc sse using spatial approach (averages of Blythe valley for now)
        """
        self.SSe = np.zeros((self.lenInfected, self.nI0))
        self.pIij = np.empty(self.nI0)
        self.seUij = np.empty(self.nI0)
        self.seUj = np.empty(self.nI0)

        for i in range(self.nI0):

            piij = self.I0[i] / self.farmCells    # prob infect of ls animal i infected in cell j; uniform across farm
            self.pIij[i] = piij
#            print('piij', piij)
            self.seUij[i] = self.pIij[i] * self.seTest
            self.seUj[i] = 1 - (1 - self.seUij[i])**423
#            print('iiiiiiiiiiiiiii', i)
#            print('I0[i], farmCells', self.I0[i], self.farmCells)
#            print('farmCells', self.I0[i] / self.farmCells)
            print('piij', self.pIij)
            print('seUij', self.seUij)
            print('seUj', self.seUj)

            sseTmp = 1 - (1 - self.seUj[i] * self.prp)**self.infected
#            print('sseTmp', sseTmp)
            self.SSe[:,i] = sseTmp

        print('SSe', self.SSe)


    def plotFX(self):
        """
        plot results
        """
        P.plot(self.infected, self.SSe[:, 2], label = 'Transmission probability = 0.25', color = 'b', linewidth = 4.5)
        P.plot(self.infected, self.SSe[:, 1], label = 'Transmission probability = 0.10', color = 'r', linewidth = 4.5)
        P.plot(self.infected, self.SSe[:, 0], label = 'Transmission probability= 0.01', color = 'k', linewidth = 4.5)
        P.ylim([0,1])
        P.xlabel('Number of infected cells; design prevalence', fontsize = 17)
        P.ylabel('System sensitivity', fontsize = 17)
        P.legend(loc='upper right')
        P.show()


########            Main function
#######
def main():

    #livestockpath = os.getenv('PREDATORPROJDIR', default = '.')
    # paths and data to read in
#    habitatDatFile = 'habData2.csv' # os.path.join(livestockpath,'habData2.csv')
#    livestockDatFile = 'spatialSentinel2.csv' #os.path.join(livestockpath,'livestocktestdata2.csv')


    spatialrun = SpatialRun()

if __name__ == '__main__':
    main()
